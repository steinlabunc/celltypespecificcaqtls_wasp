REST ChIP-seq data in H1 cells and neurons different from H1 cells 
from ENCODE portal (https://www.encodeproject.org/) 
identifiers: ENCSR000BTV and ENCSR000BHM.
