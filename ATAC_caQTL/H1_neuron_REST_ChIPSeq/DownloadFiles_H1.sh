#!/bin/bash

#SBATCH -p general
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem=16g
#SBATCH -t 24:00:00
#https://www.encodeproject.org/experiments/ENCSR000BHM/
##download
wget https://www.encodeproject.org/files/ENCFF687USK/@@download/ENCFF687USK.bam;
wget https://www.encodeproject.org/files/ENCFF945BSS/@@download/ENCFF945BSS.bam;

