#!/bin/bash

#SBATCH -p general
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem=16g
#SBATCH -t 24:00:00
#https://www.encodeproject.org/experiments/ENCSR000BTV/
##download
wget https://www.encodeproject.org/files/ENCFF000OWQ/@@download/ENCFF000OWQ.bam;
wget https://www.encodeproject.org/files/ENCFF000OWM/@@download/ENCFF000OWM.bam;

