#!/bin/bash
#SBATCH -n 1
#SBATCH -t 24:00:00
#SBATCH --mem 5120

module add shapeit;

chrs=`seq 1 22`;
for chr in $chrs; 
do 
##1) submit jobs for each chr 

sbatch -o PrePhaseSHAPEIT.$chr.out ./PrePhaseSHAPEIT.sh $chr; 

done

exit 0;










