#!/bin/bash
#SBATCH -n 16
#SBATCH -t 24:00:00
#SBATCH --mem 30720

##https://genome.sph.umich.edu/wiki/Minimac3_Cookbook_:_Pre-Phasing
## chrX and chrY in plink is chr23 and chr24
## here we impute autosome vcf files first
#chrs=`seq 1 22`;
geneticmapdir='/proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/Imputation/PrePhasedFiles/1000GP_Phase3';
vcffiledir='/proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/Imputation/VcfFiles';
#for chr in $chrs; 
#do 
##1) make vcf files in each chr
shapeit -V $vcffiledir/allmgergedGenoMafHweQC.$1.recode.vcf -M $geneticmapdir/genetic_map_chr$1_combined_b37.txt -O allmgergedGenoMafHweQC.$1.phased --thread 16;
shapeit -convert --input-haps allmgergedGenoMafHweQC.$1.phased --output-vcf $vcffiledir/allmgergedGenoMafHweQC.$1.phased.vcf;
#done

exit 0;










