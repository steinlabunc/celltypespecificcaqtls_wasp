#!/bin/bash
#SBATCH -n 1
#SBATCH -t 24:00:00
#SBATCH --mem 30720

## here we impute chrX
refgenomedir='/proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/Imputation/Minimac4RefGenome';
vcffiledir='/proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/Imputation/VcfFiles';
#for chr in $chrs; 
#do 
##1) Non-PAR
#minimac4 --refHaps $refgenomedir/X.Non.Pseudo.Auto.1000g.Phase3.v5.With.Parameter.Estimates.m3vcf --haps $vcffiledir/allmgergedGenoMafHweQC.X.Non.PAR.phased.vcf --prefix allmgergedGenoMafHweQC.X.Non.PAR --ignoreDuplicates;

##2) PAR
minimac4 --refHaps $refgenomedir/X.Pseudo.Auto.1000g.Phase3.v5.With.Parameter.Estimates.m3vcf --haps $vcffiledir/allmgergedGenoMafHweQC.X.PAR.phased.vcf --prefix allmgergedGenoMafHweQC.X.PAR --ignoreDuplicates --chunkLengthMb 60;

#done

exit 0;










