##plot all R2
library(MASS);
library(ggplot2);
library(viridis);
options(stringsAsFactors=FALSE);
theme_set(theme_bw(base_size = 16));
##Neuron Progenitor eQTLs
##PeQTLs_wp NeQTLs_wp
load("NeuronProgenitoreQTLswithR2.Rdata");
##Fetal brian eQTLs
##eQTLs_wp
##Neuron Progennitor caQTLs
##NcaQTLs_wp PcaQTLs_wp
load("../FetalBraineQTLs/QTLswithR2.Rdata");
##set quantile 10% and 90%
q=quantile(NcaQTLs_wp$R2,probs = c(0.1,0.9));
NcaQTLs_wp=NcaQTLs_wp[which(NcaQTLs_wp$R2 > q[1] & NcaQTLs_wp$R2 < q[2]),];
q=quantile(PcaQTLs_wp$R2,probs = c(0.1,0.9));
PcaQTLs_wp=PcaQTLs_wp[which(PcaQTLs_wp$R2 > q[1] & PcaQTLs_wp$R2 < q[2]),];
q=quantile(NeQTLs_wp$R2,probs = c(0.1,0.9));
NeQTLs_wp=NeQTLs_wp[which(NeQTLs_wp$R2 > q[1] & NeQTLs_wp$R2 < q[2]),];
q=quantile(PeQTLs_wp$R2,probs = c(0.1,0.9));
PeQTLs_wp=PeQTLs_wp[which(PeQTLs_wp$R2 > q[1] & PeQTLs_wp$R2 < q[2]),];
q=quantile(eQTLs_wp$R2,probs = c(0.1,0.9));
eQTLs_wp=eQTLs_wp[which(eQTLs_wp$R2 > q[1] & eQTLs_wp$R2 < q[2]),];

pdf("R2_all_QTLs.pdf",width=8,height=5);
plotdata=data.frame(x=c(NcaQTLs_wp$R2,PcaQTLs_wp$R2,NeQTLs_wp$R2,PeQTLs_wp$R2,eQTLs_wp$R2),y=c(rep("NcaQTLs",length(NcaQTLs_wp$R2)),rep("PcaQTLs",length(PcaQTLs_wp$R2)),rep("NeQTLs",length(NeQTLs_wp$R2)),rep("PeQTLs",length(PeQTLs_wp$R2)),rep("Fetal Brain eQTLs",length(eQTLs_wp$R2))));
p <- ggplot(plotdata, aes(x=y, y=x, fill=y)) + geom_violin()+ geom_boxplot(width=0.05)+ggtitle("R2 comparison");
print(p);
dev.off();

##the same donor number 
##Neuron Progennitor caQTLs
##NcaQTLs_wp PcaQTLs_wp
load("../FetalBraineQTLs/QTLswithR2.Rdata");
##fetal brain eQTLs with the same donor number of caQTLs
##PeQTLs_wp NeQTLs_wp
load("../FetalBraineQTLs/FetalBraineQTLswithR2_sameNwithcaQTL.Rdata");
FetalPeQTLs_wp=PeQTLs_wp;
FetalNeQTLs_wp=NeQTLs_wp;
##neuron progenitor eQTLs with the same donor number of caQTLs
##PeQTLs_wp NeQTLs_wp
load("NeuronProgenitoreQTLswithR2_sameNwithcaQTL.Rdata");

##set quantile 10% and 90%
q=quantile(NcaQTLs_wp$R2,probs = c(0.1,0.9));
NcaQTLs_wp=NcaQTLs_wp[which(NcaQTLs_wp$R2 > q[1] & NcaQTLs_wp$R2 < q[2]),];
q=quantile(PcaQTLs_wp$R2,probs = c(0.1,0.9));
PcaQTLs_wp=PcaQTLs_wp[which(PcaQTLs_wp$R2 > q[1] & PcaQTLs_wp$R2 < q[2]),];
q=quantile(NeQTLs_wp$R2,probs = c(0.1,0.9));
NeQTLs_wp=NeQTLs_wp[which(NeQTLs_wp$R2 > q[1] & NeQTLs_wp$R2 < q[2]),];
q=quantile(PeQTLs_wp$R2,probs = c(0.1,0.9));
PeQTLs_wp=PeQTLs_wp[which(PeQTLs_wp$R2 > q[1] & PeQTLs_wp$R2 < q[2]),];
q=quantile(FetalNeQTLs_wp$R2,probs = c(0.1,0.9));
FetalNeQTLs_wp=FetalNeQTLs_wp[which(FetalNeQTLs_wp$R2 > q[1] & FetalNeQTLs_wp$R2 < q[2]),];
q=quantile(FetalPeQTLs_wp$R2,probs = c(0.1,0.9));
FetalPeQTLs_wp=FetalPeQTLs_wp[which(FetalPeQTLs_wp$R2 > q[1] & FetalPeQTLs_wp$R2 < q[2]),];
pdf("R2_all_QTLs_sameDonorNumbers.pdf",width=8,height=5);
plotdata=data.frame(x=c(NcaQTLs_wp$R2,PcaQTLs_wp$R2,NeQTLs_wp$R2,PeQTLs_wp$R2,FetalNeQTLs_wp$R2,FetalPeQTLs_wp$R2),y=c(rep("NcaQTLs",length(NcaQTLs_wp$R2)),rep("PcaQTLs",length(PcaQTLs_wp$R2)),rep("NeQTLs",length(NeQTLs_wp$R2)),rep("PeQTLs",length(PeQTLs_wp$R2)),rep("Fetal Brain eQTLs N=61",length(FetalNeQTLs_wp$R2)),rep("Fetal Brain eQTLs N=76",length(FetalPeQTLs_wp$R2))));
p <- ggplot(plotdata, aes(x=y, y=x, fill=y)) + geom_violin()+ geom_boxplot(width=0.05)+ggtitle("R2 comparison");
print(p);
dev.off();
