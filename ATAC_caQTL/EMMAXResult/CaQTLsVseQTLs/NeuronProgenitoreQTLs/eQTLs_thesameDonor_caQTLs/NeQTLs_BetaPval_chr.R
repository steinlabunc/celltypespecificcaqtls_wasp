options(stringsAsFactors=F);
args = commandArgs(trailingOnly=TRUE);
##neuron eQTLs
dir="/proj/steinlab/projects/R00/eQTLanalysis/Raw_data/cell/caQTL/neuron/";
##output
Noutput=data.frame();
##chr
chrs=paste0("chr",c(1:22,"X.Non.PAR","X.PAR"));
#for (i in 1:length(chrs)){
    i=as.numeric(args[1]);
    files=list.files(paste0(dir,chrs[i]),pattern=glob2rx("*.ps"));
    for (j in 1:length(files)){
        name=unlist(strsplit(files[j],".",fixed=T))[1];
        name=unlist(strsplit(name,"-",fixed=T))[3];
        thisresult=read.table(paste0(dir,chrs[i],"/",files[j]),header=T);
        thisresult$gene=name;
        Noutput=rbind(Noutput,thisresult);
    }
#}
save(Noutput,file=paste0("NeQTLs_BetaPval_chr",chrs[i],".Rdata"));



