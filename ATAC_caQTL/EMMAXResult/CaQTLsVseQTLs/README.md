progenitor/neuron caQTLs vs progenitor/neuron/fetal cortical eQTLs
Compare 
1) shared dgree between eQTLs and caQTLs
2) estimated pi1 for shared SNPs in eQTLs and caQTLs
3) estimated R2 for shared SNPs in eQTLs and caQTLs

Compare
1) shared dgree between subsampled eQTLs and caQTLs
2) estimated pi1 for shared SNPs in subsampled eQTLs and caQTLs
3) estimated R2 for shared SNPs in subsampled eQTLs and caQTLs
