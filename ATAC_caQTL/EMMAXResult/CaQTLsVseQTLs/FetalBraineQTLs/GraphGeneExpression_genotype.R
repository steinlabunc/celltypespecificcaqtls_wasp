##plot ETFDH (ENSG00000171503), FABP7 (ENSG00000164434), SETD9 (ENSG00000155542), AL12182.1 (ENSG00000258636) expression and genotype graph
options(stringsAsFactors=F);
SNPs=c("chr4:158667824:A:C","chr6:122833438:C:T","chr5:56909530:A:G","chr14:41605321:A:G");
genes=c("ETFDH", "FABP7", "SETD9", "AL12182.1");
gene_ids=c("ENSG00000171503","ENSG00000164434","ENSG00000155542","ENSG00000258636");
results=c("/proj/steinlab/projects/R00/eQTLanalysis/Raw_data/bulk/chr4/F-chr4-ENSG00000171503.ps","/proj/steinlab/projects/R00/eQTLanalysis/Raw_data/bulk/chr6/F-chr6-ENSG00000164434.ps","/proj/steinlab/projects/R00/eQTLanalysis/Raw_data/bulk/chr5/F-chr5-ENSG00000155542.ps","/proj/steinlab/projects/R00/eQTLanalysis/Raw_data/bulk/chr14/F-chr14-ENSG00000258636.ps");
pheno=c("/proj/steinlab/projects/R00/eQTLanalysis/phenofiles/fetal/F-chr4-ENSG00000171503.txt","/proj/steinlab/projects/R00/eQTLanalysis/phenofiles/fetal/F-chr6-ENSG00000164434.txt","/proj/steinlab/projects/R00/eQTLanalysis/phenofiles/fetal/F-chr5-ENSG00000155542.txt","/proj/steinlab/projects/R00/eQTLanalysis/phenofiles/fetal/F-chr14-ENSG00000258636.txt");
geno=c("/proj/steinlab/projects/R00/eQTLanalysis/genofiles/allele_order_corrected_genofiles/bulk/chr4_F.uniq.tped","/proj/steinlab/projects/R00/eQTLanalysis/genofiles/allele_order_corrected_genofiles/bulk/chr6_F.uniq.tped","/proj/steinlab/projects/R00/eQTLanalysis/genofiles/allele_order_corrected_genofiles/bulk/chr5_F.uniq.tped","/proj/steinlab/projects/R00/eQTLanalysis/genofiles/allele_order_corrected_genofiles/bulk/chr14_F.uniq.tped");
bim=c("/proj/steinlab/projects/R00/eQTLanalysis/genofiles/allele_order_corrected_genofiles/bulk/chr4_F.uniq.bim","/proj/steinlab/projects/R00/eQTLanalysis/genofiles/allele_order_corrected_genofiles/bulk/chr6_F.uniq.bim","/proj/steinlab/projects/R00/eQTLanalysis/genofiles/allele_order_corrected_genofiles/bulk/chr5_F.uniq.bim","/proj/steinlab/projects/R00/eQTLanalysis/genofiles/allele_order_corrected_genofiles/bulk/chr14_F.uniq.bim");
pdf("GeneExpression_genotype.pdf");
for (i in 1:length(SNPs)) {
    thisresult=read.table(results[i],header=F);
    beta=thisresult$V2[which(thisresult$V1==SNPs[i])];
    p=thisresult$V3[which(thisresult$V1==SNPs[i])];
    chrgeno=read.table(geno[i],header=F);
    thisgeno=chrgeno[which(chrgeno$V2==SNPs[i]),];
    thisgeno=thisgeno[,5:length(thisgeno)];
    thisbim=read.table(bim[i],header=F);
    A1=thisbim$V5[which(thisbim$V2==SNPs[i])];
    A2=thisbim$V6[which(thisbim$V2==SNPs[i])];
    thisgeno[which(thisgeno==1)]=A1;
    thisgeno[which(thisgeno==2)]=A2; 
    A1A2=paste(thisgeno[seq(1,length(thisgeno),by=2)],thisgeno[seq(2,length(thisgeno),by=2)],sep="/");
    A1A2=factor(A1A2,levels=c(paste(A1,A1,sep="/"),paste(A1,A2,sep="/"),paste(A2,A2,sep="/")));
    cat("SNP",SNPs[i],"...\n");
    thispheno=read.table(pheno[i],header=F);
    boxplot(thispheno$V3~A1A2,main=paste0(" Fetal brain eSNP=",SNPs[i]," \neGene=",genes[i]," ",gene_ids[i],"\n Beta=",beta,", Pvalue=",p),ylab="RNA-seq VST counts");
    stripchart(thispheno$V3~A1A2, vertical = TRUE, add =TRUE,pch=16,method = "jitter",col = 'blue');    
}
##ATAC-seq box plot for chr4:158667824:A:C
geno=read.table("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/TruncatedFiles/Progenitor/chr4/chr4_158667771_158667860.tped",header=F);
thisgeno=geno[which(geno$V2==SNPs[1]),];
thisgeno=thisgeno[,5:length(thisgeno)];
A1="A";
A2="C";
thisgeno[which(thisgeno==1)]=A1;
thisgeno[which(thisgeno==2)]=A2;
A1A2=paste(thisgeno[seq(1,length(thisgeno),by=2)],thisgeno[seq(2,length(thisgeno),by=2)],sep="/");
A1A2=factor(A1A2,levels=c(paste(A1,A1,sep="/"),paste(A1,A2,sep="/"),paste(A2,A2,sep="/")));
phen=read.table("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Phenotype_ATAC/CSAW/Progenitor/chr4/chr4_158667771_158667860_Progenitor.txt",header=F);
boxplot(phen$V3~A1A2,main=paste0("Progenitor caSNP=",SNPs[1]," \ncaPeak=4_158667771_158667860 \n Beta=0.2662684279, Pvalue=4.305032607e-08"),ylab="ATAC VST counts");
stripchart(phen$V3~A1A2, vertical = TRUE, add =TRUE,pch=16,method = "jitter",col = 'blue');

dev.off();










