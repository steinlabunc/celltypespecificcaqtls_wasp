##plot all R2
library(MASS);
library(ggplot2);
library(viridis);
options(stringsAsFactors=FALSE);
theme_set(theme_bw(base_size = 16));
##caQTL and eQTLs
load("FetalBraineQTLswithR2_sameNwithcaQTL.Rdata");
NcaQTLs=NeQTLs_wp[,c(1,5:7,18)];
NeQTLs=NeQTLs_wp[,c(1,8,17)];
PcaQTLs=PeQTLs_wp[,c(1,5:7,18)];
PeQTLs=PeQTLs_wp[,c(1,8,17)];
pdf("R2_shared_QTLs.pdf",width=8,height=5);
plotdata=data.frame(x=c(NcaQTLs$caR2,NeQTLs$eR2,PcaQTLs$caR2,PeQTLs$eR2),y=c(rep("NcaQTLs",length(NcaQTLs$caR2)),rep("NeQTLs",length(NeQTLs$eR2)),rep("PcaQTLs",length(PcaQTLs$caR2)),rep("PeQTLs",length(PeQTLs$eR2))));
p <- ggplot(plotdata, aes(x=y, y=x, fill=y)) + geom_violin()+ geom_boxplot(width=0.05)+ggtitle("R2 comparison");
print(p);
dev.off();
