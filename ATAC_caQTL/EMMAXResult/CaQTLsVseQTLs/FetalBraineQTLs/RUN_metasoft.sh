#!/bin/bash

#SBATCH -p general
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem=16g
#SBATCH -t 10:00:00

java -jar /proj/steinlab/projects/sharedApps/Metasoft/Metasoft.jar -pvalue_table /proj/steinlab/projects/sharedApps/Metasoft/HanEskinPvalueTable.txt -input NeuronCaQTL_FetaleQTL_sharedSNPs.txt -mvalue -output NeuronCaQTL_FetaleQTL_sharedSNPs_mvalue.txt -log NeuronCaQTL_FetaleQTL_sharedSNPs_mvalue.out;

java -jar /proj/steinlab/projects/sharedApps/Metasoft/Metasoft.jar -pvalue_table /proj/steinlab/projects/sharedApps/Metasoft/HanEskinPvalueTable.txt -input ProgenitorCaQTL_FetaleQTL_sharedSNPs.txt -mvalue -output ProgenitorCaQTL_FetaleQTL_sharedSNPs_mvalue.txt -log ProgenitorCaQTL_FetaleQTL_sharedSNPs_mvalue.out;

exit 0;

