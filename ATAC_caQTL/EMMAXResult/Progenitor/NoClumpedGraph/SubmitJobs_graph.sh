#!/bin/bash

#SBATCH -p steinlab
#SBATCH -n 1
#SBATCH --mem=4g
#SBATCH -t 4:00:00

module add r/3.4.1;

for i in `seq 1 22`

do

sbatch -o GraphESandPValue_chr$i.out ./slaveR.sh GraphESandPValue_chr.R $i;

done
