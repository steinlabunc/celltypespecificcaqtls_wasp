options(stringsAsFactors=FALSE);

files=list.files(".",pattern=glob2rx("*.Rdata"));

totalNbetapval=data.frame();

totalPbetapval=data.frame();

for (i in 1:length(files)){
cat("chr",files[i],"...\n");
load(files[i]);
totalNbetapval=rbind(totalNbetapval,Nbetapval);  
totalPbetapval=rbind(totalPbetapval,Pbetapval);

}    

Nbetapval=totalNbetapval;
Pbetapval=totalPbetapval;
save(Pbetapval,Nbetapval,file="NPbetaPMatrix_CSAW_8ATACPCAs.Rdata");
Nbetapval$FDR=p.adjust(Nbetapval$Pval, method = "fdr", n = length(Nbetapval$Pval));
Pbetapval$FDR=p.adjust(Pbetapval$Pval, method = "fdr", n = length(Pbetapval$Pval));

cat("Neuron caQTLs p-value threshold when FDR<0.05 ",max(Nbetapval$Pval[Nbetapval$FDR<0.05]),"...\n");
cat("Progenitor caQTLs p-value threshold when FDR<0.05 ",max(Pbetapval$Pval[Pbetapval$FDR<0.05]),"...\n");

sigNbetapval=Nbetapval[which(Nbetapval$FDR<0.05),];
sigPbetapval=Pbetapval[which(Pbetapval$FDR<0.05),];

save(sigPbetapval,sigNbetapval,file="sigNPbetaPMatrix_CSAW_8ATACPCAs.Rdata");
