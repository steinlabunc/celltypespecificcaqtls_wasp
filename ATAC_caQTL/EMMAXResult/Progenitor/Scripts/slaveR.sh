#!/bin/bash
#SBATCH -p general
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem=4g
#SBATCH -t 8:00:00

file=$1
R --slave --args $* < $file
exit 0
