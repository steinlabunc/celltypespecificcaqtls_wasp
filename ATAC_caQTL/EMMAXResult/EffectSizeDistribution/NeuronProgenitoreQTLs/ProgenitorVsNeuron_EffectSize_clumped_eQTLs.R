library(MASS)
library(ggplot2)
library(viridis)
library(ggrastr);
options(stringsAsfactors=F);
theme_set(theme_bw(base_size = 16));
##https://slowkow.com/notes/ggplot2-color-by-density/
# Get density of points in 2 dimensions.
# @param x A numeric vector.
# @param y A numeric vector.
# @param n Create a square n by n grid to compute density.
# @return The density within each square.
get_density <- function(x, y, ...) {
  dens <- MASS::kde2d(x, y, ...)
  ix <- findInterval(x, dens$x)
  iy <- findInterval(y, dens$y)
  ii <- cbind(ix, iy)
  return(dens$z[ii])
}
##clumped neuron
NeQTLs=read.table("/proj/steinlab/projects/R00/eQTLanalysis/Clumped_data/significant_clumped/neuron.sig.genes.eigenMT.txt",header=T);
###clumped progenitor
PeQTLs=read.table("/proj/steinlab/projects/R00/eQTLanalysis/Clumped_data/significant_clumped/progenitor.sig.genes.eigenMT.txt",header=T);
##find the same neuron eQTL pairs in progenitor
##find the same progenitor eQTL pairs in neuron
Prawdir="/proj/steinlab/projects/R00/eQTLanalysis/Raw_data/cell/progenitor/";
Nrawdir="/proj/steinlab/projects/R00/eQTLanalysis/Raw_data/cell/neuron/";
##neuron
#NeQTLs$Chr=sapply(NeQTLs$snp, function(x) unlist(strsplit(as.character(x),":"))[1]);
#chrs=unique(NeQTLs$Chr);
##output
#Noutput=data.frame();
#for (i in 1:length(chrs)){
#    if (chrs[i] != "chrX"){
#       thisNeQTLs=NeQTLs[which(NeQTLs$Chr==chrs[i]),];
#       genes=unique(thisNeQTLs$gene);
#       for (j in 1:length(genes)){
#           thisthisNeQTLs=thisNeQTLs[which(thisNeQTLs$gene==genes[j]),];
#           resultfile=paste0(Prawdir,chrs[i],"/P-",chrs[i],"-",genes[j],".ps");
#           if (file.exists(resultfile)){
#              results=read.table(resultfile,header=T);
#              sharedSNPs=intersect(results$SNP,thisthisNeQTLs$snp);
#              if (length(sharedSNPs) > 0){
#                  sharedresults=results[match(sharedSNPs,results$SNP),];
#                  colnames(sharedresults)=paste0("P_",colnames(sharedresults));
#                  sharedNeQTLs=thisthisNeQTLs[match(sharedSNPs,thisthisNeQTLs$snp),];
#                  colnames(sharedNeQTLs)=paste0("N_",colnames(sharedNeQTLs));
#                  thisoutput=cbind(sharedNeQTLs,sharedresults);
#                  Noutput=rbind(Noutput,thisoutput);
#              }
#           }
#       }
#    }else {
#       thisNeQTLs=NeQTLs[which(NeQTLs$Chr==chrs[i]),];
#       genes=unique(thisNeQTLs$gene);    
#       for (j in 1:length(genes)){
#           resultfile1=paste0(Prawdir,"chrX.PAR/P-chrX.PAR-",genes[j],".ps");
#           resultfile2=paste0(Prawdir,"chrX.Non.PAR/P-chrX.Non.PAR-",genes[j],".ps");
#           resultfile=c(resultfile1,resultfile2)[which(file.exists(c(resultfile1,resultfile2)))];
#           if (length(resultfile) > 0){
#              results=read.table(resultfile,header=T);
#              sharedSNPs=intersect(results$SNP,thisthisNeQTLs$snp);
#              if (length(sharedSNPs) > 0){
#                  sharedresults=results[match(sharedSNPs,results$SNP),];
#                  colnames(sharedresults)=paste0("P_",colnames(sharedresults));
#                  sharedNeQTLs=thisthisNeQTLs[match(sharedSNPs,thisthisNeQTLs$snp),];
#                  colnames(sharedNeQTLs)=paste0("N_",colnames(sharedNeQTLs));
#                  thisoutput=cbind(sharedNeQTLs,sharedresults);
#                  Noutput=rbind(Noutput,thisoutput);
#              }
#           }   
#       }
#    }
#}
##progenitor
#PeQTLs$Chr=sapply(PeQTLs$snp, function(x) unlist(strsplit(as.character(x),":"))[1]);
#chrs=unique(PeQTLs$Chr);
##output
#Poutput=data.frame();
#for (i in 1:length(chrs)){
#    if (chrs[i] != "chrX"){
#       thisPeQTLs=PeQTLs[which(PeQTLs$Chr==chrs[i]),];
#       genes=unique(thisPeQTLs$gene);
#       for (j in 1:length(genes)){
#           thisthisPeQTLs=thisPeQTLs[which(thisPeQTLs$gene==genes[j]),];
#           resultfile=paste0(Nrawdir,chrs[i],"/N-",chrs[i],"-",genes[j],".ps");
#           if (file.exists(resultfile)){
#              results=read.table(resultfile,header=T);
#              sharedSNPs=intersect(results$SNP,thisthisPeQTLs$snp);
#              if (length(sharedSNPs) > 0){
#                  sharedresults=results[match(sharedSNPs,results$SNP),];
#                  colnames(sharedresults)=paste0("N_",colnames(sharedresults));
#                  sharedPeQTLs=thisthisPeQTLs[match(sharedSNPs,thisthisPeQTLs$snp),];
#                  colnames(sharedPeQTLs)=paste0("P_",colnames(sharedPeQTLs));
#                  thisoutput=cbind(sharedPeQTLs,sharedresults);
#                  Poutput=rbind(Poutput,thisoutput);
#              }
#           }
#       }
#    }else {
#       thisPeQTLs=PeQTLs[which(PeQTLs$Chr==chrs[i]),];
#       genes=unique(thisPeQTLs$gene);
#       for (j in 1:length(genes)){
#           resultfile1=paste0(Nrawdir,"chrX.PAR/N-chrX.PAR-",genes[j],".ps");
#           resultfile2=paste0(Nrawdir,"chrX.Non.PAR/N-chrX.Non.PAR-",genes[j],".ps");
#           resultfile=c(resultfile1,resultfile2)[which(file.exists(c(resultfile1,resultfile2)))];
#           if (length(resultfile) > 0){
#              results=read.table(resultfile,header=T);
#              sharedSNPs=intersect(results$SNP,thisthisPeQTLs$snp);
#              if (length(sharedSNPs) > 0){
#                  sharedresults=results[match(sharedSNPs,results$SNP),];
#                  colnames(sharedresults)=paste0("N_",colnames(sharedresults));
#                  sharedPeQTLs=thisthisPeQTLs[match(sharedSNPs,thisthisPeQTLs$snp),];
#                  colnames(sharedPeQTLs)=paste0("P_",colnames(sharedPeQTLs));
#                  thisoutput=cbind(sharedPeQTLs,sharedresults);
#                  Poutput=rbind(Poutput,thisoutput);
#              }
#           }
#       }
#    }
#}
#save(Poutput,Noutput,file="NeuronProgenitor_shared_eQTLs.Rdata");
load("NeuronProgenitor_shared_eQTLs.Rdata");

Ndonor=74;
Pdonor=85;
Noutput$N_SE <- abs((Noutput$N_beta)/(qt((Noutput$N_pval)/2, df = 52)));
Noutput$P_SE <- abs(as.numeric(as.character(Noutput$P_BETA))/(qt(as.numeric(as.character(Noutput$P_P))/2, df = 63)));

Poutput$P_SE <- abs((Poutput$P_beta)/(qt((Poutput$P_pval)/2, df = 63)));
Poutput$N_SE <- abs(as.numeric(as.character(Poutput$N_BETA))/(qt(as.numeric(as.character(Poutput$N_P))/2, df = 52)));

Ndata=data.frame(IDs=Noutput$N_comb,NeQTL_beta=Noutput$N_beta,NeQTL_SE=Noutput$N_SE,PeQTL_beta=Noutput$P_BETA,PeQTL_SE=Noutput$P_SE);
Pdata=data.frame(IDs=Poutput$P_comb,PeQTL_beta=Poutput$P_beta,PeQTL_SE=Poutput$P_SE,NeQTL_beta=Poutput$N_BETA,NeQTL_SE=Poutput$N_SE);

write.table(Ndata,file="sigNeuroneQTL_withsamePeQTLs.txt",quote=F,row.names=F,col.names=F);
write.table(Pdata,file="sigProgenitoreQTL_withsameNeQTLs.txt",quote=F,row.names=F,col.names=F);

##Neuron
NeQTLs_wp=data.frame();
Ndata$SNP=sapply(Ndata$IDs,function(x) unlist(strsplit(as.character(x),"_"))[1]);
Ndata$chr=sapply(Ndata$SNP,function(x) unlist(strsplit(x,":"))[1]);
Ndata$chr=sapply(Ndata$chr, function(x) substr(x,4,nchar(x)));
Nchrs=unique(Ndata$chr);
for (i in 1:length(Nchrs)){
    thisNdata=Ndata[which(Ndata$chr==Nchrs[i]),];
    thisNfreq=read.table(paste0("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/CaQTLsVseQTLs/NeuronProgenitoreQTLs/MAF_files/",Nchrs[i],".dose.R2g03.QC.Neuron.frq"),header=T);
    thisPfreq=read.table(paste0("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/CaQTLsVseQTLs/NeuronProgenitoreQTLs/MAF_files/",Nchrs[i],".dose.R2g03.QC.Progenitor.frq"),header=T);
    Nindex=match(thisNdata$SNP,thisNfreq$SNP);
    Pindex=match(thisNdata$SNP,thisPfreq$SNP);
    thisNdata$NA2=thisNfreq$A2[Nindex];
    thisNdata$PA2=thisPfreq$A2[Pindex];
    NeQTLs_wp=rbind(NeQTLs_wp,thisNdata);
}
##Progenitor
PeQTLs_wp=data.frame();
Pdata$SNP=sapply(Pdata$IDs,function(x) unlist(strsplit(as.character(x),"_"))[1]);
Pdata$chr=sapply(Pdata$SNP,function(x) unlist(strsplit(x,":"))[1]);
Pdata$chr=sapply(Pdata$chr, function(x) substr(x,4,nchar(x)));
Pchrs=unique(Pdata$chr);
for (i in 1:length(Pchrs)){
    thisPdata=Pdata[which(Pdata$chr==Pchrs[i]),];
    thisPfreq=read.table(paste0("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/CaQTLsVseQTLs/NeuronProgenitoreQTLs/MAF_files/",Pchrs[i],".dose.R2g03.QC.Progenitor.frq"),header=T);
    thisNfreq=read.table(paste0("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/CaQTLsVseQTLs/NeuronProgenitoreQTLs/MAF_files/",Pchrs[i],".dose.R2g03.QC.Neuron.frq"),header=T);
    Pindex=match(thisPdata$SNP,thisPfreq$SNP);
    Nindex=match(thisPdata$SNP,thisNfreq$SNP);
    thisPdata$PA2=thisPfreq$A2[Pindex];
    thisPdata$NA2=thisNfreq$A2[Nindex];
    PeQTLs_wp=rbind(PeQTLs_wp,thisPdata);
}

##plot beta value
pdf("CompareEffectSizeNeuronProgenitoreQTLs.pdf");
sign=rep(1,dim(NeQTLs_wp)[1]);
sign[which(as.character(NeQTLs_wp$NA2) != as.character(NeQTLs_wp$PA2))]=-1;
plotdata=data.frame(SNP=NeQTLs_wp$IDs,x=as.numeric(as.character(NeQTLs_wp$NeQTL_beta)),y=as.numeric(as.character((NeQTLs_wp$PeQTL_beta)))*sign);
plotdata$density <- get_density(plotdata$x, plotdata$y, n = 100);
Nplotdata=plotdata;
save(Nplotdata,file="ProgenitorVsNeuron_EffectSize_clumped_plot.Rdata");
gg_rast=ggplot(plotdata,aes(x,y,color=density))+geom_point_rast(size=0.1)+scale_color_viridis()+geom_abline(intercept = 0, slope = 1, color="red",linetype="dashed")+geom_smooth(method='lm',col="red")+labs(title = paste0("Effect size compare eQTLs Cor=",cor(plotdata$x,plotdata$y)),x = "Neuron Sig eQTLs",y = "Progenitor eQTLs common with Neuron sig eQTLs");
print(gg_rast);

sign=rep(1,dim(PeQTLs_wp)[1]);
sign[which(as.character(PeQTLs_wp$NA2) != as.character(PeQTLs_wp$PA2))]=-1;
plotdata=data.frame(SNP=PeQTLs_wp$IDs,x=as.numeric(as.character(PeQTLs_wp$PeQTL_beta)),y=as.numeric(as.character((PeQTLs_wp$NeQTL_beta)))*sign);
plotdata$density <- get_density(plotdata$x, plotdata$y, n = 100);
Pplotdata=plotdata;
save(Nplotdata,Nplotdata,file="ProgenitorVsNeuron_EffectSize_clumped_plot.Rdata");
gg_rast=ggplot(plotdata,aes(x,y,color=density))+geom_point_rast(size=0.1)+scale_color_viridis()+geom_abline(intercept = 0, slope = 1, color="red",linetype="dashed")+geom_smooth(method='lm',col="red")+labs(title = paste0("Effect size compare eQTLs Cor=",cor(plotdata$x,plotdata$y)),x = "Progenitor Sig eQTLs",y = "Neuron eQTLs common with Progenitor sig eQTLs");
print(gg_rast);

dev.off();

