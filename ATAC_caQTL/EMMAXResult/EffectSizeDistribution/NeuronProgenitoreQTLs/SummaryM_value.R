options(stringsAsFactors=FALSE);
##all clumped eQTLs
##neuron
Ndata=read.table("sigNeuroneQTL_withsamePeQTLs_mvalue.txt",skip=1);
N_n=length(which(Ndata$V19>0.9 & Ndata$V20>0.9));
cat("all clumped eQTLs: # shared Neuron eQTLs with progenitor eQTLs is", N_n,"\n");
cat("all clumped eQTLs: Ratio shared Neuron eQTLs with progenitor eQTLs is", N_n/dim(Ndata)[1],"\n");

##progenitor
Pdata=read.table("sigProgenitoreQTL_withsameNeQTLs_mvalue.txt",skip=1);
P_n=length(which(Pdata$V19>0.9 & Pdata$V20>0.9));
cat("all clumped eQTLs: # shared Progenitor eQTLs with Neuron eQTLs is", P_n,"\n");
cat("all clumped eQTLs: Ratio shared Progenitor eQTLs with Neuron eQTLs is", P_n/dim(Pdata)[1],"\n");

##unique clumped eQTLs
##neuron
Ndata=read.table("sigNeuroneQTL_withsamePeQTLs_mvalue_unique.txt",skip=1);
N_n=length(which(Ndata$V19>0.9 & Ndata$V20>0.9));
cat("unique clumped eQTLs: # shared Neuron eQTLs with progenitor eQTLs is", N_n,"\n");
cat("unique clumped eQTLs: Ratio shared Neuron eQTLs with progenitor eQTLs is", N_n/dim(Ndata)[1],"\n");

##progenitor
Pdata=read.table("sigProgenitoreQTL_withsameNeQTLs_mvalue_unique.txt",skip=1);
P_n=length(which(Pdata$V19>0.9 & Pdata$V20>0.9));
cat("unique clumped eQTLs: # shared Progenitor eQTLs with Neuron eQTLs is", P_n,"\n");
cat("unique clumped eQTLs: Ratio shared Progenitor eQTLs with Neuron eQTLs is", P_n/dim(Pdata)[1],"\n");



