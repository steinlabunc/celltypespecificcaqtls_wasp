#!/bin/bash

#SBATCH -p general
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem=16g
#SBATCH -t 10:00:00
##all
java -jar /proj/steinlab/projects/sharedApps/Metasoft/Metasoft.jar -pvalue_table /proj/steinlab/projects/sharedApps/Metasoft/HanEskinPvalueTable.txt -input sigNeuroneQTL_withsamePeQTLs.txt -mvalue -output sigNeuroneQTL_withsamePeQTLs_mvalue.txt  -log sigNeuroneQTL_withsamePeQTLs.out;

java -jar /proj/steinlab/projects/sharedApps/Metasoft/Metasoft.jar -pvalue_table /proj/steinlab/projects/sharedApps/Metasoft/HanEskinPvalueTable.txt -input sigProgenitoreQTL_withsameNeQTLs.txt -mvalue -output sigProgenitoreQTL_withsameNeQTLs_mvalue.txt -log sigProgenitoreQTL_withsameNeQTLs.out;

##unique pair

java -jar /proj/steinlab/projects/sharedApps/Metasoft/Metasoft.jar -pvalue_table /proj/steinlab/projects/sharedApps/Metasoft/HanEskinPvalueTable.txt -input sigNeuroneQTL_withsamePeQTLs_unique.txt -mvalue -output sigNeuroneQTL_withsamePeQTLs_mvalue_unique.txt  -log sigNeuroneQTL_withsamePeQTLs_unique.out;

java -jar /proj/steinlab/projects/sharedApps/Metasoft/Metasoft.jar -pvalue_table /proj/steinlab/projects/sharedApps/Metasoft/HanEskinPvalueTable.txt -input sigProgenitoreQTL_withsameNeQTLs_unique.txt -mvalue -output sigProgenitoreQTL_withsameNeQTLs_mvalue_unique.txt -log sigProgenitoreQTL_withsameNeQTLs_unique.out;
exit 0;

