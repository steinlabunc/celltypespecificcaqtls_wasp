#!/bin/bash

#SBATCH -p general
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem=16g
#SBATCH -t 10:00:00
##unique
java -jar /proj/steinlab/projects/sharedApps/Metasoft/Metasoft.jar -pvalue_table /proj/steinlab/projects/sharedApps/Metasoft/HanEskinPvalueTable.txt -input sigNcaQTLsVsPcaQTLs.txt -mvalue -output sigNcaQTLsVsPcaQTLs_mvalue.txt -log sigNcaQTLsVsPcaQTLs_mvalue.out;

java -jar /proj/steinlab/projects/sharedApps/Metasoft/Metasoft.jar -pvalue_table /proj/steinlab/projects/sharedApps/Metasoft/HanEskinPvalueTable.txt -input sigPcaQTLsVsNcaQTLs.txt -mvalue -output sigPcaQTLsVsNcaQTLs_mvalue.txt -log sigPcaQTLsVsNcaQTLs_mvalue.out;

##all
java -jar /proj/steinlab/projects/sharedApps/Metasoft/Metasoft.jar -pvalue_table /proj/steinlab/projects/sharedApps/Metasoft/HanEskinPvalueTable.txt -input sigNcaQTLsVsPcaQTLs_all.txt -mvalue -output sigNcaQTLsVsPcaQTLs_mvalue_all.txt -log sigNcaQTLsVsPcaQTLs_mvalue_all.out;

java -jar /proj/steinlab/projects/sharedApps/Metasoft/Metasoft.jar -pvalue_table /proj/steinlab/projects/sharedApps/Metasoft/HanEskinPvalueTable.txt -input sigPcaQTLsVsNcaQTLs_all.txt -mvalue -output sigPcaQTLsVsNcaQTLs_mvalue_all.txt -log sigPcaQTLsVsNcaQTLs_mvalue_all.out;

exit 0;

