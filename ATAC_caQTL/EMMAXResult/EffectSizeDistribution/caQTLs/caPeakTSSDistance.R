library(GenomicRanges);
library(GenomicFeatures);
library(GenomicAlignments);
library(DESeq2);
library(ggplot2);
options(stringsAsFactors=FALSE);
##load reads counts matrix
load("/proj/steinlab/projects/R00/ATACpreprocess/DifferentChromAccess/CSAW/Invitro/Limma/Limma_RemvBatch_CSAWcounts.Rdata");
correctedcounts=assay(vsd);
##read differentical peaks
diffbind=read.csv("/proj/steinlab/projects/R00/ATACpreprocess/DifferentChromAccess/CSAW/Invitro/Limma/ATAC_NeuronProgenitor_DifferentialPeaks.csv");
diffbindGR = GRanges(seqname=diffbind$Chr,IRanges(diffbind$peakstart,diffbind$peakend));
mcols(diffbindGR) = diffbind[,4:9];
allpeakID=paste(diffbind$Chr,diffbind$peakstart,diffbind$peakend,sep="_");
##set row names for the count matrix
rownames(correctedcounts)=allpeakID;
##promoter regions
load("/proj/steinlab/projects/R00/ATACpreprocess/DifferentChromAccess/GOAnalysis/Script/All_protcodepromt.Rdata");
olaps=findOverlaps(protcodepromt,diffbindGR);
promotcounts=correctedcounts[unique(subjectHits(olaps)),];
##caQTLs
NcaQTLs=read.csv("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Neuron/CSAW/PCAs_7/ClumpedGraph/Neuron_caQTLs_closest_clumped_PC1_7.csv");
PcaQTLs=read.csv("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Progenitor/CSAW/PCAs_8/ClumpedGraph/Progenitor_caQTLs_closest_clumped_PC1_8.csv");
NcaPeaks=unique(paste0(NcaQTLs$SNP,"_chr",NcaQTLs$seqnames,"_",NcaQTLs$peakstart,"_",NcaQTLs$peakend));
PcaPeaks=unique(paste0(PcaQTLs$SNP,"_chr",PcaQTLs$seqnames,"_",PcaQTLs$peakstart,"_",PcaQTLs$peakend));
SharedcaPeaks=intersect(NcaPeaks,PcaPeaks);
NcaPeaks=setdiff(NcaPeaks,SharedcaPeaks);
PcaPeaks=setdiff(PcaPeaks,SharedcaPeaks);
SharedcaPeaks=unique(paste(sapply(SharedcaPeaks,function(x) unlist(strsplit(x,"_"))[2]),sapply(SharedcaPeaks,function(x) unlist(strsplit(x,"_"))[3]),sapply(SharedcaPeaks,function(x) unlist(strsplit(x,"_"))[4]),sep="_"));
NcaPeaks=unique(paste(sapply(NcaPeaks,function(x) unlist(strsplit(x,"_"))[2]),sapply(NcaPeaks,function(x) unlist(strsplit(x,"_"))[3]),sapply(NcaPeaks,function(x) unlist(strsplit(x,"_"))[4]),sep="_"));
PcaPeaks=unique(paste(sapply(PcaPeaks,function(x) unlist(strsplit(x,"_"))[2]),sapply(PcaPeaks,function(x) unlist(strsplit(x,"_"))[3]),sapply(PcaPeaks,function(x) unlist(strsplit(x,"_"))[4]),sep="_"));
##caQTLs with peaks habour caSNP
NcaQTLsInpeak=NcaQTLs[which(NcaQTLs$InPeak),];
PcaQTLsInpeak=PcaQTLs[which(PcaQTLs$InPeak),];
NcaPeaksIn=unique(paste0(NcaQTLsInpeak$SNP,"_chr",NcaQTLsInpeak$seqnames,"_",NcaQTLsInpeak$peakstart,"_",NcaQTLsInpeak$peakend));
PcaPeaksIn=unique(paste0(PcaQTLsInpeak$SNP,"_chr",PcaQTLsInpeak$seqnames,"_",PcaQTLsInpeak$peakstart,"_",PcaQTLsInpeak$peakend));
SharedcaPeaksIn=intersect(NcaPeaksIn,PcaPeaksIn);
NcaPeaksIn=setdiff(NcaPeaksIn,SharedcaPeaksIn);
PcaPeaksIn=setdiff(PcaPeaksIn,SharedcaPeaksIn);
SharedcaPeaksIn=unique(paste(sapply(SharedcaPeaksIn,function(x) unlist(strsplit(x,"_"))[2]),sapply(SharedcaPeaksIn,function(x) unlist(strsplit(x,"_"))[3]),sapply(SharedcaPeaksIn,function(x) unlist(strsplit(x,"_"))[4]),sep="_"));
NcaPeaksIn=unique(paste(sapply(NcaPeaksIn,function(x) unlist(strsplit(x,"_"))[2]),sapply(NcaPeaksIn,function(x) unlist(strsplit(x,"_"))[3]),sapply(NcaPeaksIn,function(x) unlist(strsplit(x,"_"))[4]),sep="_"));
PcaPeaksIn=unique(paste(sapply(PcaPeaksIn,function(x) unlist(strsplit(x,"_"))[2]),sapply(PcaPeaksIn,function(x) unlist(strsplit(x,"_"))[3]),sapply(PcaPeaksIn,function(x) unlist(strsplit(x,"_"))[4]),sep="_"));
##SharedcaPeaksIn
SInplotdata=data.frame(TSSDistance=rep(NA,length(SharedcaPeaksIn)),Type=rep("Shared_caPeak_wSNP",length(SharedcaPeaksIn)));
rownames(SInplotdata)=SharedcaPeaksIn;
chrs=unique(sapply(SharedcaPeaksIn,function(x) unlist(strsplit(x,"_"))[1]));
for (i in 1:length(chrs)){
    thisSharedcaPeaksIn=SharedcaPeaksIn[grep(paste0(chrs[i],"_"),SharedcaPeaksIn)]; 
    thischrpromotcounts=promotcounts[grep(paste0(chrs[i],"_"),rownames(promotcounts))];    
    for (j in 1:length(thisSharedcaPeaksIn)){
        thiscounts=correctedcounts[which(allpeakID==thisSharedcaPeaksIn[j]),];
        thiscors=apply(promotcounts,1,function(x) cor(x,thiscounts));
        centers=(as.numeric(sapply(rownames(promotcounts),function(x) unlist(strsplit(x,"_"))[2]))+as.numeric(sapply(rownames(promotcounts),function(x) unlist(strsplit(x,"_"))[3])))/2;
        TSSdis=abs(centers-((as.numeric(unlist(strsplit(thisSharedcaPeaksIn[j],"_"))[2])+as.numeric(unlist(strsplit(thisSharedcaPeaksIn[j],"_"))[3]))/2));
        thiscors=thiscors[which(TSSdis <= 1000000)];
        thiscenters=centers[which(TSSdis <= 1000000)];
        thisTSSdis=TSSdis[which(TSSdis <= 1000000)];
        SInplotdata$TSSDistance[which(SharedcaPeaksIn==thisSharedcaPeaksIn[j])]=unique(thisTSSdis[which(thiscors==max(thiscors))]);
}
}
##NcaPeaksIn
NInplotdata=data.frame(TSSDistance=rep(NA,length(NcaPeaksIn)),Type=rep("Neuron_caPeak_wSNP",length(NcaPeaksIn)));
rownames(NInplotdata)=NcaPeaksIn;
chrs=unique(sapply(NcaPeaksIn,function(x) unlist(strsplit(x,"_"))[1]));
for (i in 1:length(chrs)){
    thisNcaPeaksIn=NcaPeaksIn[grep(paste0(chrs[i],"_"),NcaPeaksIn)];
    thischrpromotcounts=promotcounts[grep(paste0(chrs[i],"_"),rownames(promotcounts))];
    for (j in 1:length(thisNcaPeaksIn)){
        thiscounts=correctedcounts[which(allpeakID==thisNcaPeaksIn[j]),];
        thiscors=apply(promotcounts,1,function(x) cor(x,thiscounts));
        centers=(as.numeric(sapply(rownames(promotcounts),function(x) unlist(strsplit(x,"_"))[2]))+as.numeric(sapply(rownames(promotcounts),function(x) unlist(strsplit(x,"_"))[3])))/2;
        TSSdis=abs(centers-((as.numeric(unlist(strsplit(thisNcaPeaksIn[j],"_"))[2])+as.numeric(unlist(strsplit(thisNcaPeaksIn[j],"_"))[3]))/2));
        thiscors=thiscors[which(TSSdis <= 1000000)];
        thiscenters=centers[which(TSSdis <= 1000000)];
        thisTSSdis=TSSdis[which(TSSdis <= 1000000)];
        NInplotdata$TSSDistance[which(NcaPeaksIn==thisNcaPeaksIn[j])]=unique(thisTSSdis[which(thiscors==max(thiscors))]);
}
}
##PcaPeaksIn
PInplotdata=data.frame(TSSDistance=rep(NA,length(PcaPeaksIn)),Type=rep("Progenitor_caPeak_wSNP",length(PcaPeaksIn)));
rownames(PInplotdata)=PcaPeaksIn;
chrs=unique(sapply(PcaPeaksIn,function(x) unlist(strsplit(x,"_"))[1]));
for (i in 1:length(chrs)){
    thisPcaPeaksIn=PcaPeaksIn[grep(paste0(chrs[i],"_"),PcaPeaksIn)];
    thischrpromotcounts=promotcounts[grep(paste0(chrs[i],"_"),rownames(promotcounts))];
    for (j in 1:length(thisPcaPeaksIn)){
        thiscounts=correctedcounts[which(allpeakID==thisPcaPeaksIn[j]),];
        thiscors=apply(promotcounts,1,function(x) cor(x,thiscounts));
        centers=(as.numeric(sapply(rownames(promotcounts),function(x) unlist(strsplit(x,"_"))[2]))+as.numeric(sapply(rownames(promotcounts),function(x) unlist(strsplit(x,"_"))[3])))/2;
        TSSdis=abs(centers-((as.numeric(unlist(strsplit(thisPcaPeaksIn[j],"_"))[2])+as.numeric(unlist(strsplit(thisPcaPeaksIn[j],"_"))[3]))/2));
        thiscors=thiscors[which(TSSdis <= 1000000)];
        thiscenters=centers[which(TSSdis <= 1000000)];
        thisTSSdis=TSSdis[which(TSSdis <= 1000000)];
        PInplotdata$TSSDistance[which(PcaPeaksIn==thisPcaPeaksIn[j])]=unique(thisTSSdis[which(thiscors==max(thiscors))]);
}
}
##plotdata
save(SInplotdata,NInplotdata,PInplotdata,file="Inplotdata.Rdata");
Inplotdata=rbind(SInplotdata,NInplotdata,PInplotdata);
Inplotdata$TSSDistance=as.numeric(Inplotdata$TSSDistance)/1000;
g=ggplot(Inplotdata, aes(x=TSSDistance, color=Type)) + geom_density() + labs(title="caPeak_wSNP",x="Distance from caPeaks_wSNP to TSS", y = "Density");
pdf("caPeaksTSSDistance.pdf");
print(g);
Inplotdata$Peak="Enhancer";
Inplotdata$Peak[which(Inplotdata$TSSDistance==0)]="Promoter";
p<-ggplot(data=Inplotdata, aes(x=Type, y=Peak,color=Peak)) +geom_bar(stat="identity");
plot(p);



##SharedcaPeaks
Splotdata=data.frame(TSSDistance=rep(NA,length(SharedcaPeaks)),Type=rep("Shared_caPeak",length(SharedcaPeaks)));
rownames(Splotdata)=SharedcaPeaks;
chrs=unique(sapply(SharedcaPeaks,function(x) unlist(strsplit(x,"_"))[1]));
for (i in 1:length(chrs)){
    thisSharedcaPeaks=SharedcaPeaks[grep(paste0(chrs[i],"_"),SharedcaPeaks)];
    thischrpromotcounts=promotcounts[grep(paste0(chrs[i],"_"),rownames(promotcounts))];
    for (j in 1:length(thisSharedcaPeaks)){
        thiscounts=correctedcounts[which(allpeakID==thisSharedcaPeaks[j]),];
        thiscors=apply(promotcounts,1,function(x) cor(x,thiscounts));
        centers=(as.numeric(sapply(rownames(promotcounts),function(x) unlist(strsplit(x,"_"))[2]))+as.numeric(sapply(rownames(promotcounts),function(x) unlist(strsplit(x,"_"))[3])))/2;
        TSSdis=abs(centers-((as.numeric(unlist(strsplit(thisSharedcaPeaks[j],"_"))[2])+as.numeric(unlist(strsplit(thisSharedcaPeaks[j],"_"))[3]))/2));
        thiscors=thiscors[which(TSSdis <= 1000000)];
        thiscenters=centers[which(TSSdis <= 1000000)];
        thisTSSdis=TSSdis[which(TSSdis <= 1000000)];
        Splotdata$TSSDistance[which(SharedcaPeaks==thisSharedcaPeaks[j])]=thisTSSdis[which(thiscors==max(thiscors))];
}
}
##NcaPeaks
Nplotdata=data.frame(TSSDistance=rep(NA,length(NcaPeaks)),Type=rep("Neuron_caPeak",length(NcaPeaks)));
rownames(Nplotdata)=NcaPeaks;
chrs=unique(sapply(NcaPeaks,function(x) unlist(strsplit(x,"_"))[1]));
for (i in 1:length(chrs)){
    thisNcaPeaks=NcaPeaks[grep(paste0(chrs[i],"_"),NcaPeaks)];
    thischrpromotcounts=promotcounts[grep(paste0(chrs[i],"_"),rownames(promotcounts))];
    for (j in 1:length(thisNcaPeaks)){
        thiscounts=correctedcounts[which(allpeakID==thisNcaPeaks[j]),];
        thiscors=apply(promotcounts,1,function(x) cor(x,thiscounts));
        centers=(as.numeric(sapply(rownames(promotcounts),function(x) unlist(strsplit(x,"_"))[2]))+as.numeric(sapply(rownames(promotcounts),function(x) unlist(strsplit(x,"_"))[3])))/2;
        TSSdis=abs(centers-((as.numeric(unlist(strsplit(thisNcaPeaks[j],"_"))[2])+as.numeric(unlist(strsplit(thisNcaPeaks[j],"_"))[3]))/2));
        thiscors=thiscors[which(TSSdis <= 1000000)];
        thiscenters=centers[which(TSSdis <= 1000000)];
        thisTSSdis=TSSdis[which(TSSdis <= 1000000)];
        Nplotdata$TSSDistance[which(NcaPeaks==thisNcaPeaks[j])]=thisTSSdis[which(thiscors==max(thiscors))];
}
}
##PcaPeaks
Pplotdata=data.frame(TSSDistance=rep(NA,length(PcaPeaks)),Type=rep("Progenitor_caPeak",length(PcaPeaks)));
rownames(Pplotdata)=PcaPeaks;
chrs=unique(sapply(PcaPeaks,function(x) unlist(strsplit(x,"_"))[1]));
for (i in 1:length(chrs)){
    thisPcaPeaks=PcaPeaks[grep(paste0(chrs[i],"_"),PcaPeaks)];
    thischrpromotcounts=promotcounts[grep(paste0(chrs[i],"_"),rownames(promotcounts))];
    for (j in 1:length(thisPcaPeaks)){
        thiscounts=correctedcounts[which(allpeakID==thisPcaPeaks[j]),];
        thiscors=apply(promotcounts,1,function(x) cor(x,thiscounts));
        centers=(as.numeric(sapply(rownames(promotcounts),function(x) unlist(strsplit(x,"_"))[2]))+as.numeric(sapply(rownames(promotcounts),function(x) unlist(strsplit(x,"_"))[3])))/2;
        TSSdis=abs(centers-((as.numeric(unlist(strsplit(thisPcaPeaks[j],"_"))[2])+as.numeric(unlist(strsplit(thisPcaPeaks[j],"_"))[3]))/2));
        thiscors=thiscors[which(TSSdis <= 1000000)];
        thiscenters=centers[which(TSSdis <= 1000000)];
        thisTSSdis=TSSdis[which(TSSdis <= 1000000)];
        Pplotdata$TSSDistance[which(PcaPeaks==thisPcaPeaks[j])]=thisTSSdis[which(thiscors==max(thiscors))];
}
}
##plotdata
plotdata=rbind(Splotdata,Nplotdata,Pplotdata);
plotdata$TSSDistance=as.numeric(plotdata$TSSDistance)/1000;
save(Splotdata,Nplotdata,Pplotdata,file="plotdata.Rdata");
g=ggplot(plotdata, aes(x=TSSDistance, color=Type)) + geom_density() + labs(title="caPeak",x="Distance from caPeaks to TSS", y = "Density");
print(g);

plotdata$Peak="Enhancer";
plotdata$Peak[which(plotdata$TSSDistance==0)]="Promoter";
p<-ggplot(data=plotdata, aes(x=Type, y=Peak,color=Peak)) +geom_bar(stat="identity");
plot(p);

dev.off();








