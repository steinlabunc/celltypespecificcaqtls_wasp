#!/bin/bash

#SBATCH -p general
#SBATCH -n 1
#SBATCH --mem=4g
#SBATCH -t 1:00:00

nums=`seq 1 70`;
for i in $nums
do
sbatch -n 1 --mem=15g --time=10:00:00 -p steinlab -o Neuron.$i.out --wrap="Rscript FindOverlappedSNPs_Neuron.R $i";
sbatch -n 1 --mem=15g --time=10:00:00 -p steinlab -o Progenitor.$i.out --wrap="Rscript FindOverlappedSNPs_Progenitor.R $i";
done

