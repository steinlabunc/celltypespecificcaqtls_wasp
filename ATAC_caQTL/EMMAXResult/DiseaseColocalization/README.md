Clolocalization between caQTLs and GWAS data
Using conditional caQTL test.

4 Steps:
1) SNPs have R2 >0.8 with index caSNPs (LD matrix from caQTL donors)
2) SNPs have R2 >0.8 with index GWAS SNPs (LD matrix from 1000D EUP people)
3) find overlaping for 1) and 2)
4) conditional caQTLs on index GWAS SNPs
