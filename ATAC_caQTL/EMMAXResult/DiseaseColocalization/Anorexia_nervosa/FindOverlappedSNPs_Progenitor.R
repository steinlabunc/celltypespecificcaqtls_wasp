library(GenomicRanges);
library(GenomicFeatures);
library(rtracklayer);
library(GenomicAlignments);
options(stringsAsFactors=FALSE);
##read GWAS data
GWASdata=read.table(gzfile("pgc.ed.freeze1.summarystatistics.July2017.txt.gz"),header=T);
##keep significant SNPs
GWASdata=GWASdata[which(GWASdata$P < 5e-08),];
## make data a GRanges
resultGR = GRanges(paste0("chr",GWASdata$CHR),IRanges(GWASdata$BP,GWASdata$BP));
mcols(resultGR)=GWASdata;
resultGR=unique(resultGR);
## convert hg19 to hg38
path="/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/DiseaseColocalization/CoordinatesColocalization/ENIGMA3Traits/hg19ToHg38.over.chain";
ch = import.chain(path);
hg38resultGR=unlist(liftOver(resultGR,ch));
save(hg38resultGR,file="sigGWAS_SNP_coords_hg38.Rdata");
##seqnames
chrs=intersect(unique(GWASdata$CHR),1:22);
##save results
output=GRanges();
for (i in 1:length(chrs)){
    ##read in NcaSNPs within peaks and with r2>0.8
    PcaSNPs=read.csv(paste0("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/DiseaseColocalization/ConditinalCaQTLs/TestCaSNPs/Progenitor/SNPs_LD_thresdold_chr",chrs[i],".csv"));
    colnames(PcaSNPs)[8]="chrnames";
    PcaSNPs_A=GRanges(paste0("chr",PcaSNPs$CHR_A),IRanges(PcaSNPs$BP_A,PcaSNPs$BP_A));
    olaps=findOverlaps(PcaSNPs_A, hg38resultGR);
    olapA=hg38resultGR[subjectHits(olaps)];
    mcols(olapA)=cbind(mcols(olapA),PcaSNPs[queryHits(olaps),]);
    
    PcaSNPs_B=GRanges(paste0("chr",PcaSNPs$CHR_B),IRanges(PcaSNPs$BP_B,PcaSNPs$BP_B));
    olaps=findOverlaps(PcaSNPs_B, hg38resultGR);
    olapB=hg38resultGR[subjectHits(olaps)];
    mcols(olapB)=cbind(mcols(olapB),PcaSNPs[queryHits(olaps),]);
     
    olap=unique(c(olapA,olapB));
    if (length(olap)>0){
       cat("Got ",length(olap)," SNPs overlapped on chr",i,"...\n");
       output=c(output,olap);
    }
}
if (length(output)>0){
   save(output,file="OverlappedSNPs_Progenitor.Rdata");
}




