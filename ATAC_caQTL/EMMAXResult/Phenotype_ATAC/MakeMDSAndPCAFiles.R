library(DESeq2);
options(stringsAsFactors=FALSE);
##load corrected vst count number
load("/proj/steinlab/projects/R00/ATACpreprocess/DifferentChromAccess/CSAW/Invitro/DESeq2/CSAW_100L_dds.Rdata");
vsd <- vst(dds);
counts=assay(vsd);
##load meta data
fnamesamplesheet = "/proj/steinlab/projects/R00/ATACpreprocess/DifferentChromAccess/CSAW/Invitro/DESeq2/ATAC_NeuronProgenitor.csv";
samplesheet = read.csv(fnamesamplesheet,header=TRUE);
colnames(counts)=samplesheet$SampleID;
##PCAs
Nindex=which(samplesheet$CellType=="Neuron");
Pindex=which(samplesheet$CellType=="Progenitor");
pca_Ncounts <- prcomp(t(counts[,Nindex]));
pca_Pcounts <- prcomp(t(counts[,Pindex]));
NPCAs=pca_Ncounts$x;
PPCAs=pca_Pcounts$x;
count_NIDs=paste(samplesheet$DonorID,samplesheet$DNAID,sep="_")[Nindex];
count_PIDs=paste(samplesheet$DonorID,samplesheet$DNAID,sep="_")[Pindex];
sorter=samplesheet$Sorter[Nindex];
## keep PCs files as the same order as genotype files
MDSs=read.table("/proj/steinlab/projects/R00/atac-qtl/GenotypeData/PlinkData_original/QCMergedData/MDSs/MDSs10.mds.emmax");
IDs=paste0(MDSs$V1,"_",MDSs$V2);
## keep PCA files as the same order as genotype files
Nfam=read.table("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Phenotype_ATAC/Ntest.fam");
NIDs=paste0(Nfam$V1,"_",Nfam$V2);
Pfam=read.table("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Phenotype_ATAC/Ptest.fam");
PIDs=paste0(Pfam$V1,"_",Pfam$V2);
##neuron
NMDSs=MDSs[match(NIDs,IDs),];
NPCAs=NPCAs[match(NIDs,count_NIDs),];
##progenitor
PMDSs=MDSs[match(PIDs,IDs),];
PPCAs=PPCAs[match(PIDs,count_PIDs),];
## write down 1st to 20th PCs
for (i in 1:20){
    ##Neuron
    ncol=dim(NMDSs)[2];
    outputNMDSs=NMDSs;
    outputNMDSs[,(ncol+1):(ncol+i)]=NPCAs[,1:i];
    outputNMDSs$Sorter=as.numeric(as.factor(sorter[match(NIDs,count_NIDs)]))-1;
    write.table(outputNMDSs,file=paste0("MDSs10.Neuron.PCA1_",i,".Sorter.emmax"),quote=F,row.names=F,col.names=F);
    ##Progenitor
    ncol=dim(PMDSs)[2];
    outputPMDSs=PMDSs;
    outputPMDSs[,(ncol+1):(ncol+i)]=PPCAs[,1:i];
    write.table(outputPMDSs,file=paste0("MDSs10.Progenitor.PCA1_",i,".emmax"),quote=F,row.names=F,col.names=F);
}







