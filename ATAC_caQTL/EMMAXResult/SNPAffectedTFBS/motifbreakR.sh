#!/bin/bash
#SBATCH -n 1
#SBATCH --time=4:00:00
#SBATCH --mem=8g

module add r/3.6.0;

num=`seq 1 3095`;
for i in $num
do

sbatch -n 1 --mem=16g --time=10:00:00 -o ./logfiles/motifbreakR.$i.out --wrap="Rscript motifbreakR.R $i";

done 
