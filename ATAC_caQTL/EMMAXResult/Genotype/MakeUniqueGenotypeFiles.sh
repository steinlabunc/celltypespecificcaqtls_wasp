#!/bin/bash

#SBATCH -p general
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem=4g
#SBATCH -t 40:00

module add plink/1.90b3;
# $1 is the chr number

dir="/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38";

#first round
#plink --tfile $dir/$1.dose.R2g03.QC --list-duplicate-vars --out $1.dose.R2g03.QC;

#cut -f4 $1.dose.R2g03.QC.dupvar | cut -f1 -d" " > $1.dose.R2g03.QC.dupvar.list;

#plink --bfile $dir/$1.dose.R2g03.QC --exclude $1.dose.R2g03.QC.dupvar.list --make-bed --out $1.dose.R2g03.QC.uniq;
#second round (still have duplicate IDs after first round, becuase more than 2 IDs are the same in the same SNP position)

plink --bfile $dir/$1.dose.R2g03.QC.uniq --list-duplicate-vars --out $1.dose.R2g03.QC.uniq;

cut -f4 $1.dose.R2g03.QC.uniq.dupvar | cut -f1 -d" " > $1.dose.R2g03.QC.dupvar.uniq.list;

plink --bfile $dir/$1.dose.R2g03.QC.uniq --exclude $1.dose.R2g03.QC.dupvar.uniq.list --make-bed --out $1.dose.R2g03.QC.uniq2;

mv $1.dose.R2g03.QC.uniq2.bim $1.dose.R2g03.QC.uniq.bim;
mv $1.dose.R2g03.QC.uniq2.fam $1.dose.R2g03.QC.uniq.fam;
mv $1.dose.R2g03.QC.uniq2.bed $1.dose.R2g03.QC.uniq.bed;
mv *.nosex;
exit 0;

