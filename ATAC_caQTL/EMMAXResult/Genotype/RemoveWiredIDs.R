options(stringsAsFactors=F);
chrs=c(1:22);
for (i in 1:length(chrs)){
    thisbim=read.table(paste0(chrs[i],".dose.R2g03.QC.bim"));
    thisIDs=data.frame(IDs=thisbim$V2[which(thisbim$V1!=chrs[i])]);
    write.table(thisIDs,file=paste0(chrs[i],".remove.list"),quote=F,row.names=F);
    thisbim$V1=chrs[i];
    write.table(thisbim,file=paste0(chrs[i],".dose.R2g03.QC.bim"),quote=F,sep="\t",row.names=F,col.names=F);
}

