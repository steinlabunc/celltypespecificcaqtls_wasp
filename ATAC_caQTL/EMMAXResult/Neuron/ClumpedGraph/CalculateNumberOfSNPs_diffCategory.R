## used index SNPs and their LD Buddies (r^2 = 1 or 0.5<= r^2 <1)
library(DESeq2);
library(GenomicRanges);
options(stringsAsFactors=FALSE);
## include index SNPs LD buddies (0.5 <= r^2 < =1)
ClumpedNcaQTLs_wLD=read.csv("Neuron_caQTLs_clumped_wLD_SNPs_PC1_7.csv");
ClumpedNcaQTLs_wLD$B_BP=sapply(ClumpedNcaQTLs_wLD$SNP_B, function(x) unlist(strsplit(x,":"))[2]);
ClumpedNcaQTLs_wLD$B_InPeak=(ClumpedNcaQTLs_wLD$B_BP >= ClumpedNcaQTLs_wLD$peakstart & ClumpedNcaQTLs_wLD$B_BP <= ClumpedNcaQTLs_wLD$peakend);
ClumpedNcaQTLs_wLD$SNPPeakIDs=paste(ClumpedNcaQTLs_wLD$SNP,ClumpedNcaQTLs_wLD$peakid,sep="_");
caPeakGR=GRanges(ClumpedNcaQTLs_wLD$seqnames,IRanges(ClumpedNcaQTLs_wLD$peakstart,ClumpedNcaQTLs_wLD$peakend));
mcols(caPeakGR)=ClumpedNcaQTLs_wLD$peakid;
caPeakGR=unique(caPeakGR);
## #Index caSNP and #Index caSNP with SNPs 0.5<= r^2 <=1 in its caPeak
C1=unique(paste0(ClumpedNcaQTLs_wLD$SNP,"_",ClumpedNcaQTLs_wLD$peakid)[which(ClumpedNcaQTLs_wLD$B_InPeak)]);
cat("#Index caSNP and #SNPs with 0.5<= r^2 <=1 of index SNP in its caPeak",length(C1),"...\n");
#make the LD SNPs a GRange
ClumpedNcaSNPs_wLD=GRanges(ClumpedNcaQTLs_wLD$seqnames,IRanges(as.numeric(ClumpedNcaQTLs_wLD$B_BP),as.numeric(ClumpedNcaQTLs_wLD$B_BP)));
ClumpedNcaPeaks_wLD=GRanges(ClumpedNcaQTLs_wLD$seqnames,IRanges(ClumpedNcaQTLs_wLD$peakstart,ClumpedNcaQTLs_wLD$peakend));
##LD budies overlapping caPeaks
polap=findOverlaps(ClumpedNcaSNPs_wLD,ClumpedNcaPeaks_wLD);
index=which(queryHits(polap)!=subjectHits(polap));
C2=unique(paste(ClumpedNcaQTLs_wLD$SNP[queryHits(polap)],ClumpedNcaQTLs_wLD$peakid[queryHits(polap)],sep="_")[index]);
C2=setdiff(C2,intersect(C2,C1));
#cat("#SNPs with Index caSNP 0.5<= r^2 <=1 in other caPeak",length(C2),"...\n");
##aimed SNPs
cor_ClumpedNcaQTLs_wLD=ClumpedNcaQTLs_wLD[which(ClumpedNcaQTLs_wLD$SNPPeakIDs %in% C2),];
cor_ClumpedNcaQTLs_wLD=cor_ClumpedNcaQTLs_wLD[which(cor_ClumpedNcaQTLs_wLD$B_InPeak=="FALSE"),];
B_SNPGR=GRanges(cor_ClumpedNcaQTLs_wLD$seqnames,IRanges(as.numeric(cor_ClumpedNcaQTLs_wLD$B_BP),as.numeric(cor_ClumpedNcaQTLs_wLD$B_BP)));
##overlap
olaps=findOverlaps(B_SNPGR,caPeakGR);
olapcor_ClumpedNcaQTLs_wLD=cor_ClumpedNcaQTLs_wLD[queryHits(olaps),];
olapcor_ClumpedNcaQTLs_wLD$cor_Peak=paste(seqnames(caPeakGR),start(caPeakGR),end(caPeakGR),sep="_")[subjectHits(olaps)];
##Peaks
corPeaks=unique(data.frame(SNP=olapcor_ClumpedNcaQTLs_wLD$SNP,leadPeak=olapcor_ClumpedNcaQTLs_wLD$peakid,corPeak=olapcor_ClumpedNcaQTLs_wLD$cor_Peak));
##batch corrected peak counts
load("/proj/steinlab/projects/R00/ATACpreprocess/DifferentChromAccess/CSAW/Invitro/Limma/Limma_RemvBatch_CSAWcounts.Rdata");
correctedcounts=assay(vsd);
##load meta data
fnamesamplesheet = "/proj/steinlab/projects/R00/ATACpreprocess/DifferentChromAccess/CSAW/Invitro/DESeq2/ATAC_NeuronProgenitor.csv";
samplesheet = read.csv(fnamesamplesheet,header=TRUE);
colnames(correctedcounts)=samplesheet$SampleID;
## load peak coodinates
peaks=read.csv("/proj/steinlab/projects/R00/ATACpreprocess/DifferentChromAccess/CSAW/Invitro/Limma/ATAC_NeuronProgenitor_DifferentialPeaks.csv");
peaks$seqnames=sapply(peaks$Chr, function(x) substr(x,4,nchar(as.character(x))));
peakID=paste(peaks$seqnames,peaks$peakstart,peaks$peakend,sep="_");
## keep phenotype files as the same order as genotype files
Nfam=read.table("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Phenotype_ATAC/Ntest.fam");
NIDs=paste0(Nfam$V1,"_",Nfam$V2);
## Neuron
NverifiedIDs=samplesheet[which(samplesheet$CellType=="Neuron"),];
correctedNcounts=correctedcounts[,match(NverifiedIDs$SampleID,colnames(correctedcounts))];
Ncoverage=correctedNcounts[,match(NIDs,paste(NverifiedIDs$DonorID,NverifiedIDs$DNAID,sep="_"))];
rownames(Ncoverage)=peakID;
corPeaks$cor=NA;
corPeaks$P=NA;
for (i in 1:dim(corPeaks)[1]){
    testGR=GRanges(unlist(strsplit(corPeaks$leadPeak[i],"_"))[1],IRanges(as.numeric(unlist(strsplit(corPeaks$leadPeak[i],"_"))[2])-2000000,as.numeric(unlist(strsplit(corPeaks$leadPeak[i],"_"))[3])+2000000));
    testcaPeakGR=caPeakGR[subjectHits(findOverlaps(testGR,caPeakGR))];
    testdata=data.frame(leadPeak=corPeaks$leadPeak[i],corPeak= testcaPeakGR$X[which(testcaPeakGR$X != corPeaks$leadPeak[i])],cor=NA,P=NA);
    for (j in 1:dim(testdata)[1]){
        leadpeakcounts=Ncoverage[which(rownames(Ncoverage)==testdata$leadPeak[j]),];
        corpeakcounts=Ncoverage[which(rownames(Ncoverage)==testdata$corPeak[j]),];
        thistest=cor.test(leadpeakcounts,corpeakcounts);
        testdata$cor[j]=thistest$estimate;
        testdata$P[j]=thistest$p.value;
    }
    testdata$FDR=p.adjust(testdata$P,method="fdr");
    ind=which(testdata$corPeak==corPeaks$corPeak[i]);
    corPeaks$cor[i]=testdata$cor[ind];
    corPeaks$P[i]=testdata$P[ind];
    corPeaks$FDR[i]=testdata$FDR[ind];
}
save(corPeaks,file="PeaksCor.Rdata");
C2=unique(paste(corPeaks$SNP,corPeaks$leadPeak,sep="_")[which(corPeaks$FDR<0.05)]);
cat("#Index SNPs+#SNPs with Index caSNP 0.5<=r^2<=1 in correlated other caPeaks",length(C2),"...\n");
##make GRanges for open peaks in Neuron
diffacce=peaks;
diffacceGR=GRanges(diffacce$seqnames,IRanges(as.numeric(diffacce$peakstart),as.numeric(diffacce$peakend)));
mcols(diffacceGR)=diffacce;
diffacceGR=diffacceGR[which(diffacceGR$logFC<0)];
olap=findOverlaps(caPeakGR,diffacceGR);
diffacceGR=diffacceGR[-subjectHits(olap)];
olap=findOverlaps(ClumpedNcaSNPs_wLD,diffacceGR);
C3=unique(paste(ClumpedNcaQTLs_wLD$SNP,ClumpedNcaQTLs_wLD$peakid,sep="_")[queryHits(olap)]);
C3=setdiff(C3,intersect(C3,c(C1,C2)));
cat("#Index SNPs+#SNPs with Index caSNP 0.5=<r^2<=1 in other Neuron open Peak",length(C3),"...\n");
##ChromHMM states
ChromHMMfiles = "/proj/steinlab/projects/R00/ChromHMM/E081_25_imputed12marks_dense_hg38.bed";
ChromHMMfile = read.table(ChromHMMfiles);
colnames(ChromHMMfile)=c("chrom","chromStart","chromEnd","name","score","strand","thickStart","thickEnd","itemRgb");
thisstateGR = GRanges(ChromHMMfile$chrom,IRanges(ChromHMMfile$chromStart,ChromHMMfile$chromEnd));
mcols(thisstateGR)=ChromHMMfile;
##ADD SNPpeakID
ClumpedNcaQTLs_wLD$SNPPeakID=paste(ClumpedNcaQTLs_wLD$SNP,ClumpedNcaQTLs_wLD$peakid,sep="_");
SNPPeakIDs=c(C1,C2,C3);
ClumpedNcaQTLs_wLD=ClumpedNcaQTLs_wLD[-which(ClumpedNcaQTLs_wLD$SNPPeakID %in% SNPPeakIDs),];
ClumpedNSNP_wLD=GRanges(paste0("chr",ClumpedNcaQTLs_wLD$seqnames),IRanges(as.numeric(ClumpedNcaQTLs_wLD$B_BP),as.numeric(ClumpedNcaQTLs_wLD$B_BP)));
mcols(ClumpedNSNP_wLD)=ClumpedNcaQTLs_wLD$SNPPeakID;
ClumpedNSNP_wLD$States="Unknown";
olaps=findOverlaps(ClumpedNSNP_wLD,thisstateGR);
ClumpedNSNP_wLD$States[queryHits(olaps)]=thisstateGR$name[subjectHits(olaps)];
active=c("1_TssA","2_PromU","3_PromD1","4_PromD2","22_PromP","23_PromBiv","13_EnhA1","14_EnhA2","15_EnhAF","16_EnhW1","17_EnhW2","18_EnhAc","5_Tx5'","6_Tx","7_Tx3'","8_TxWk","9_TxReg","10_TxEnh5'","11_TxEnh3'","12_TxEnhW","19_DNase");
C4=unique(ClumpedNSNP_wLD[which(ClumpedNSNP_wLD$States %in% active)]$X);
cat("#Index SNPs+#SNPs with Index caSNP 0.5<=r^2<=1 in Active states",length(C4),"...\n");
save(C1,C2,C3,C4,file="CalculateNumberOfSNPs_diffCategory.Rdata");






