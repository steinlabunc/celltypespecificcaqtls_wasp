#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --time=24:00:00
#SBATCH --mem=10g

module add r/3.4.1;
module add vcftools;
module add picard;
module add gatk/4.0.3.0;

## keep unique SNPs in VCF files
vcftools --gzvcf $1.dose.R2g03.hg38.biallelic.vcf.gz --snps SNPs$1.txt --recode --out $1.dose.R2g03.hg38.bialle;
## add new cogtins
java -jar /nas/longleaf/apps/picard/2.18.22/picard-2.18.22/picard.jar UpdateVcfSequenceDictionary I=$1.dose.R2g03.hg38.bialle.recode.vcf O=$1.dose.R2g03.hg38.bial.newcogtin.vcf SEQUENCE_DICTIONARY=/pine/scr/d/a/danliang/WASPResult/hg38_UCSC_chr/genome.fa;
##sort
bcftools sort $1.dose.R2g03.hg38.bial.newcogtin.vcf -o $1.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz -O z;
##index
gatk IndexFeatureFile -F $1.dose.R2g03.hg38.bial.newcogtin.sort.vcf.gz;

## rm the temp files
rm $1.dose.R2g03.hg38.biallelic.vcf.gz;
rm $1.dose.R2g03.hg38.SNPs;
rm SNPs$1.txt;
rm $1.dose.R2g03.hg38.bialle.recode.vcf;
rm $1.dose.R2g03.hg38.bial.newcogtin.vcf;

exit 0;


