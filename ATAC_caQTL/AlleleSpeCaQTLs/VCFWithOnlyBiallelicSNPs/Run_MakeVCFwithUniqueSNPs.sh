#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --time=4:00:00
#SBATCH --mem=4g

for chr in `seq 1 22` X
do

sbatch -o MakeVCFwithUniqueSNPs.$chr.out MakeVCFwithUniqueSNPs.sh $chr;

done

exit 0;



