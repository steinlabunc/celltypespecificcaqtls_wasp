#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --time=24:00:00
#SBATCH --mem=10g

module add r/3.4.1;
module add vcftools;

## keep only bi-allele SNPs
VCFdir="/proj/steinlab/projects/R00/atac-qtl/WASPResult/GenotypeHG38CHR";
bcftools view -m2 -M2 -v snps $VCFdir/$1.dose.R2g03.hg38.vcf.gz -O z -o $1.dose.R2g03.hg38.biallelic.vcf.gz;
## extrsct SNP IDs from VCF files
zcat $1.dose.R2g03.hg38.biallelic.vcf.gz |sed  '/##/d' | cut -f1,3,2 >  $1.dose.R2g03.hg38.SNPs;
## use R to print a txt file for Unique SNP IDs
Rscript KeepUniqueSNPs.R $1; 

exit 0;

