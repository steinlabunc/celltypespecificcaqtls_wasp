#!/bin/bash
#SBATCH -n 1
#SBATCH -t 8:00:00
#SBATCH --mem 10240

module add picard/2.18.22;
module add samtools;

java -jar /nas/longleaf/apps/picard/2.18.22/picard-2.18.22/picard.jar AddOrReplaceReadGroups I=$1 O=$3 RGLB=lib1 RGPL=illumina RGPU=unit1 RGSM=$2;

samtools index $3;

exit 0;





