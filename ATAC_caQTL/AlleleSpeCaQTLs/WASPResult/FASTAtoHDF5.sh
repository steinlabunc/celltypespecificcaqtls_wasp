#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --time=24:00:00
#SBATCH --mem=100g

module add wasp;
##reference FASTA files to HDF5

/nas/longleaf/apps/wasp/2018-07/WASP/snp2h5/fasta2h5 --chrom hg38.chromInfo.txt.gz --seq reference.seq.h5 /pine/scr/d/a/danliang/WASPResult/hg38_UCSC_chr/chr*.fa.gz;

exit 0;






