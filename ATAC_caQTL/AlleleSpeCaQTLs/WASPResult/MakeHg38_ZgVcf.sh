#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --time=4:00:00
#SBATCH --mem=15g


cat /proj/steinlab/projects/R00/atac-qtl/WASPResult/GenotypeHG19/$1.dose.R2g03.header /proj/steinlab/projects/R00/atac-qtl/WASPResult/GenotypeHG38CHR/$1.dose.R2g03.hg38.data | gzip > $1.dose.R2g03.hg38.vcf.gz;

exit 0;


