#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --time=24:00:00
#SBATCH --mem=10g

awk '{print "chr"$1,$2,$2+1,$3}' /proj/steinlab/projects/R00/atac-qtl/WASPResult/GenotypeHG19/D18_DNAID490.vcf >D18_DNAID490.vcf.bed;

liftOver D18_DNAID490.vcf.bed 19ToHg38.over.chain D18_DNAID490.hg38.bed D18_DNAID490.unlifted;

awk '{print $1,$2,$4}' D18_DNAID490.hg38.bed >Hg38coordinates_SNP.txt;




