#!/bin/bash

#SBATCH -p general
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem=8g
#SBATCH -t 4:00:00

module add wasp;

python3 /nas/longleaf/apps/wasp/2018-07/WASP/mapping/filter_remapped_reads.py $1 $2 $3;


exit 0;






