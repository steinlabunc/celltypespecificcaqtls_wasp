#!/bin/bash

#SBATCH -p general
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem=8g
#SBATCH -t 4:00:00

module add wasp;

python3 /nas/longleaf/apps/wasp/2018-07/WASP/mapping/find_intersecting_snps.py --is_paired_end --is_sorted --output_dir $2 --snp_tab /proj/steinlab/projects/R00/atac-qtl/WASPResult/HDF5Files/snp_tab.h5 --snp_index /proj/steinlab/projects/R00/atac-qtl/WASPResult/HDF5Files/snp_index.h5 --haplotype /proj/steinlab/projects/R00/atac-qtl/WASPResult/HDF5Files/haplotypes.h5 --samples Samples.txt $1;







