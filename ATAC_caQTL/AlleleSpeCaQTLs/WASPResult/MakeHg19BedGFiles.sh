#!/bin/bash

#SBATCH -p general
#SBATCH -n 1
#SBATCH --mem=10g
#SBATCH -t 10:00:00

head -18 $1.dose.R2g03.vcf > $1.dose.R2g03.header;

sed 1,18d $1.dose.R2g03.vcf >  $1.dose.R2g03.data;

awk '{ print $1,$2,$2+1,$3}' $1.dose.R2g03.data > $1.dose.R2g03.bed;

exit 0;
