#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --time=24:00:00
#SBATCH --mem=10g

head -28 $1 > $2;
cat $2 $3 > $4;
rm $2;
rm $3;
exit 0;


