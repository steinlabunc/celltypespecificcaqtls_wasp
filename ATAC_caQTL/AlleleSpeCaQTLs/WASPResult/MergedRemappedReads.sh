#!/bin/bash

#SBATCH -p general
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mem=15g
#SBATCH -t 4:00:00


module add samtools;

samtools merge $3 $1 $2;
samtools sort -o $4 $3;
samtools index $4;
rm $3;
exit 0;






