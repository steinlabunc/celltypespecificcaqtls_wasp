#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --time=4:00:00
#SBATCH --mem=8g

module add r/3.4.1;

nfile=`ls /proj/steinlab/projects/R00/atac-qtl/WASPResult/GenotypeHG19/*.vcf|wc -l`;

for i in `seq 2 325`
do

sbatch -o slaveR_$i.out ./slaveR.sh MakeHg38_SNPvcf.R $i;

done

exit 0;


