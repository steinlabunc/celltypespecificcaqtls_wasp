#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --time=24:00:00
#SBATCH --mem=20g

module add vcftools;

vcffiles=`ls D*_DNAID*.1.recode.vcf`;
for file in $vcffiles
do
bgzip $file;
tabix -p vcf $file.gz;

done

vcf-merge D*_DNAID*.1.recode.vcf.gz | bgzip -c > ALL.chr1.vcf.gz;

exit 0;


