#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --time=10:00:00
#SBATCH --mem=15g

module add vcftools;
for i in `seq 1 22`
do 
vcftools --vcf $1 --chr $i --recode --recode-INFO-all --out $2.$i;
done

exit 0;





