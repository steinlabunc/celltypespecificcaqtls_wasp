#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --time=4:00:00
#SBATCH --mem=15g

zcat /proj/steinlab/projects/R00/atac-qtl/WASPResult/GenotypeHG38CHR/$1.dose.R2g03.hg38.vcf.gz | head -18 > $1.dose.R2g03.hg38.header;


zcat /proj/steinlab/projects/R00/atac-qtl/WASPResult/GenotypeHG38CHR/$1.dose.R2g03.hg38.vcf.gz|sed 1,18d > $1.dose.R2g03.hg38.temp;


awk '{$1 = "chr"$1; print}' $1.dose.R2g03.hg38.temp > $1.dose.R2g03.hg38.data;


cat $1.dose.R2g03.hg38.header $1.dose.R2g03.hg38.data | gzip > $1.dose.R2g03.hg38.vcf.gz;

rm $1.dose.R2g03.hg38.temp;
rm $1.dose.R2g03.hg38.data;
rm $1.dose.R2g03.hg38.header;

exit 0;







