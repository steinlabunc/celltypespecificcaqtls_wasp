WASP mapping: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4626402/
WASP software: https://github.com/bmvdgeijn/WASP

WASP was used for mapping ATAC-seq reads and remove the mapping bias caused by refernce genome.

