Test Allele Specific Chromatin Accessibility (ASCA) for SNPs in the ATAC-peak using DESeq2.

Thanks to the help from Dr. Michael I. Love (UNC) in this analysis. 

THreshold for SNPs in this study:
1) the number of heterogeneous donor for the SNP is >= 5
2) ALT + REF counts >= 10
3) (ALT + REF counts)/(the number of heterogeneous donor) >= 15

