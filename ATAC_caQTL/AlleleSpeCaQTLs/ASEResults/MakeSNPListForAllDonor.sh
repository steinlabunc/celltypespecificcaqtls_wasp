#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --time=4:00:00
#SBATCH --mem=10g

## make a SNP list for all donors
## 3rd column is the SNP ID

awk '{print $3,$4,$5}' ../FilteredASEReadCounts/alleleCountTable-ASEReadCounter_R00AL*.txt|uniq > SNPListForAllDonor.txt; 

exit 0;
