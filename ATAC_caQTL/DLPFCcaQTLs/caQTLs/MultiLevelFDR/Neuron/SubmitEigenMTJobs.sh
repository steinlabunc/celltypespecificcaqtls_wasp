#!/bin/bash

#SBATCH -p general
#SBATCH -n 1
#SBATCH --mem=4g
#SBATCH -t 4:00:00

for i in `seq 1 22`
do

sbatch -o slaveR_chr$i.out ./slaveR.sh MakeEigenMTFiles.R $i;

done

exit 0;
