#!/bin/bash
#SBATCH -p general
#SBATCH -n 1
#SBATCH --mem=16g
#SBATCH -t 48:00:00

file=$1
R --slave --args $* < $file
exit 0
