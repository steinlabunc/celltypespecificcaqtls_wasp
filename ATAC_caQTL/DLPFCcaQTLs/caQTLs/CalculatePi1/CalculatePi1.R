library(rtracklayer);
library(GenomicRanges);
library(qvalue);
options(stringsAsFactors=FALSE);
##regions
## change peakID (hg19) to a GRange file
DLPFC_caQTLs_hg19=read.table("/proj/steinlab/projects/R00/atac-qtl/DLPFCcaQTLs/psychENCODE_cQTL_5kb.txt",header=T);
DLPFC_caQTLs_hg19$chr=sapply(DLPFC_caQTLs_hg19$PEAK_ID,function(x) unlist(strsplit(x,"_"))[1]);
DLPFC_caQTLs_hg19$start=sapply(DLPFC_caQTLs_hg19$PEAK_ID,function(x) as.numeric(unlist(strsplit(x,"_"))[2]));
DLPFC_caQTLs_hg19$end=sapply(DLPFC_caQTLs_hg19$PEAK_ID,function(x) as.numeric(unlist(strsplit(x,"_"))[3]));
## peaks
DLPFCcaPEAKsGR=GRanges(DLPFC_caQTLs_hg19$chr, IRanges(DLPFC_caQTLs_hg19$start,DLPFC_caQTLs_hg19$end));
mcols(DLPFCcaPEAKsGR)=DLPFC_caQTLs_hg19[,1:9];
## convert hg19 to hg38
path = "/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/DiseaseColocalization/CoordinatesColocalization/ENIGMA3Traits/hg19ToHg38.over.chain";
ch = import.chain(path);
## hg38
DLPFCcaPEAKsGRhg38=unlist(liftOver(DLPFCcaPEAKsGR,ch));
##peaks
DLPFCcaPEAKsGRhg38$DLPFCcaPEAKsGRhg38IDs=paste(seqnames(DLPFCcaPEAKsGRhg38),start(DLPFCcaPEAKsGRhg38),end(DLPFCcaPEAKsGRhg38),sep="_");
##output
output=GRanges();
##chrs for SNPs
chrs=unique(DLPFCcaPEAKsGRhg38$CHR);
##SNP IDs
for (i in 1:length(chrs)){
    thisDLPFCcaPEAKsGRhg38=DLPFCcaPEAKsGRhg38[which(DLPFCcaPEAKsGRhg38$CHR==chrs[i]),];
    thisDLPFCcaPEAKsGRhg38$SNPIDs=paste0(thisDLPFCcaPEAKsGRhg38$CHR,":",thisDLPFCcaPEAKsGRhg38$SNPLOC_B37,":",thisDLPFCcaPEAKsGRhg38$REF,":",thisDLPFCcaPEAKsGRhg38$ALT);
    mySNPIDs=read.table(paste0("/proj/steinlab/projects/R00/atac-qtl/EMMAXResult/Genotype_Hg38/",chrs[i],".hg38.bed"),header=F);
    sharedSNPs=intersect(mySNPIDs$V4, thisDLPFCcaPEAKsGRhg38$SNPIDs);
    thisDLPFCcaPEAKsGRhg38=thisDLPFCcaPEAKsGRhg38[which(thisDLPFCcaPEAKsGRhg38$SNPIDs %in% sharedSNPs)];
    testSNPIDs=mySNPIDs[which(mySNPIDs$V4 %in% sharedSNPs),];
    REF=sapply(testSNPIDs$V4, function(x) unlist(strsplit(x,":",fixed=T))[3]);
    ALT=sapply(testSNPIDs$V4, function(x) unlist(strsplit(x,":",fixed=T))[4]);
    thisDLPFCcaPEAKsGRhg38$SNPIDhg38=paste(testSNPIDs$V1,testSNPIDs$V2,REF,ALT,sep=":")[match(thisDLPFCcaPEAKsGRhg38$SNPIDs,testSNPIDs$V4)];
    output=c(output,thisDLPFCcaPEAKsGRhg38);
}
##caQTL IDs
DLPFC_caQTLIDs=paste(output$DLPFCcaPEAKsGRhg38IDs,output$SNPIDhg38,sep="_");
save(output,DLPFC_caQTLIDs,file="DLPFC_caQTLIDs.Rdata");
##results
load("../NoClumpedGraph/NPbetaPMatrix.Rdata");
Nbetapval$IDs=paste0("chr",Nbetapval$seqnames,"_",Nbetapval$peakstart,"_",Nbetapval$peakend,"_",Nbetapval$SNP);
Nbetapval=Nbetapval[which(Nbetapval$IDs %in% DLPFC_caQTLIDs),];

Pbetapval$IDs=paste0("chr",Pbetapval$seqnames,"_",Pbetapval$peakstart,"_",Pbetapval$peakend,"_",Pbetapval$SNP);
Pbetapval=Pbetapval[which(Pbetapval$IDs %in% DLPFC_caQTLIDs),];

##neuron eQTLs in progenitors
Npi0 <- pi0est(p = Nbetapval$Pval, lambda = seq(0.1,0.9, 0.1), pi0.method = "smoother");
cat("Pi1 of DLPFC caQTLs in neurons is",(1-Npi0$pi0),".\n");
##progenitors eQTLs in neuron
Ppi0 <- pi0est(p = Pbetapval$Pval, lambda = seq(0.1,0.9, 0.1), pi0.method = "smoother");
cat("Pi1 of DLPFC caQTLs in progenitors is",(1-Ppi0$pi0),".\n");





