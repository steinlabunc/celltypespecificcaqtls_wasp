#!/bin/bash
#SBATCH -n 1
#SBATCH -t 24:00:00
#SBATCH --mem 30720
module add ldsc;
python /nas/longleaf/apps/ldsc/1.0.0/ldsc/ldsc.py --h2 $1 --out /proj/steinlab/projects/R00/ATACPartitionedHeritability/PartitionedHeritability/WASPCSAW/DiseaseTraits/$2 --frqfile-chr /proj/steinlab/projects/sharedApps/ldsc/1000G_frq/1000G.mac5eur. --overlap-annot --ref-ld-chr /proj/steinlab/projects/R00/ATACPartitionedHeritability/AnnotFiles/WASPCSAW/,/proj/steinlab/projects/sharedApps/ldsc/baseline/baseline. --w-ld-chr /proj/steinlab/projects/sharedApps/ldsc/weights_hm3_no_hla/weights.






