##for submit jobs
options(stringsAsFactors=FALSE);
## diseases
sumfilelist = list.files("/proj/steinlab/projects/R00/ATACPartitionedHeritability/diseasetraitmunged",pattern="*sumstats.gz",full.names=TRUE);

for (i in 1:length(sumfilelist)){
    thissum = sumfilelist[i];
    name = unlist(strsplit(thissum,"/",fixed=TRUE))[length( unlist(strsplit(thissum,"/",fixed=TRUE)))];
    name = unlist(strsplit(name,".",fixed=TRUE))[1];
    outfile = paste0(name,".out");
    system(paste0("sbatch -o ../DiseaseTraits/",outfile," ./PartitionedHeritability_ldsc_Disease.sh ",thissum," ",name));

}









