##Graph differential TF bindings
library(JASPAR2016);
library(TFBSTools);
options(stringsAsFactors=FALSE);

opts = list();
opts[["collection"]] = "CORE";
opts[["all_versions"]] = TRUE;
opts[["tax_group"]] = "vertebrates";
opts[["matrixtype"]] = "PWM";
JASPAR2016 = file.path(system.file("extdata", package="JASPAR2016"),"JASPAR2016.sqlite");
pwmatrices = getMatrixSet(JASPAR2016, opts);
TFID=ID(pwmatrices);
TFclass=matrixClass(pwmatrices);

##Read in values
TFs = read.csv('DifferentialEnrichmentTFs_logistic_conserved.csv');
##Take only most recent version of each TF
TFs$ID1 = sapply(TFs$TFID,function(x) {unlist(strsplit(x,".",fixed=TRUE))[[1]]});
TFs$version = as.numeric(sapply(TFs$TFID,function(x) {unlist(strsplit(x,".",fixed=TRUE))[[2]]}));
uniqueTFs = unique(TFs$ID1);
keepind = integer(0);
for (i in 1:length(uniqueTFs)) {
  ind = which(uniqueTFs[i]==TFs$ID1);
  if (length(ind)==1) {
    keepind = c(keepind,ind);
  } else {
    keepind = c(keepind,ind[which.max(TFs$version[ind])]);
  }
}
TFs = TFs[keepind,];  
for (j in 1:length(TFs$TFID)) {
    TFs$TF_class[j]=TFclass[which(TFID==TFs$TFID[j])];
}

##Sort by Z-score
ind = order(-(sign(TFs$estimate)*-log10(TFs$pval)));
TFs = TFs[ind,];
##FDR correct p-values
TFs$Padj = p.adjust(TFs$pval,method="fdr");
TFsig = TFs[which(TFs$Padj<0.05),];

TFsigposZ = as.data.frame(TFsig[which(TFsig$estimate>0),]);
cat(capture.output(print(TFsigposZ,row.names=F), file="TFs_sig_posZ_logi_Dan.csv"));
TFsignegZ = as.data.frame(TFsig[which(TFsig$estimate<0),]);
cat(capture.output(print(TFsignegZ,row.names=F), file="TFs_sig_negZ_logi_Dan.csv"));
output=data.frame(TFname=TFsignegZ$TFname,TFID=TFsignegZ$TFID,pval=TFsignegZ$pval,estimate=TFsignegZ$estimate,ID1=TFsignegZ$ID1,version=TFsignegZ$version,TF_class=TFsignegZ$TF_class,Padj=TFsignegZ$Padj);
write.csv(output,file="TFs_sig_negZ_logi_Dan.csv",row.names=FALSE,quote=FALSE);

pdf("DifferentialEnrichmentTFs_logistic_conserved.pdf",height=5,width=12);
barplot(sign(TFsig$estimate)*-log10(TFsig$pval),names=TFsig$TFname,las=2,ylab="-log10(P-value)",cex.names=0.25);
dev.off();


