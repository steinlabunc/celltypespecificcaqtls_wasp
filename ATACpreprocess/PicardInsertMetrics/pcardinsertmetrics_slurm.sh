#!/bin/bash
#SBATCH -n 1
#SBATCH -t 24:00:00
#SBATCH --mem 10240

## module add picard/2.10.3
## java/1.8.0_131

java -jar /nas/longleaf/apps/picard/2.10.3/picard-2.10.3/picard.jar CollectInsertSizeMetrics ASSUME_SORTED=TRUE M=0.05 I=$1 O=$2 H=$3;

exit 0;
