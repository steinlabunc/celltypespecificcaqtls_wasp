#!/bin/bash
#SBATCH -n 1
#SBATCH -t 24:00:00
#SBATCH --mem 30720

##module add bwa/0.7.15
##module add samtools/1.5

bamfiles=`find ./ATAC_CC19_NYGC -name "*_sort.bam"`;

for bam in $bamfiles
do
	echo $bam;
	samtools view -c $bam;
	echo $bam.chrM;
	samtools view $bam 'chrM'| wc -l;
done

bamfiles=`find ./ATAC_CC20_NYGC -name "*_sort.bam"`;

for bam in $bamfiles
do
        echo $bam;
        samtools view -c $bam;
        echo $bam.chrM;
        samtools view $bam 'chrM'| wc -l;
done





exit 0;
