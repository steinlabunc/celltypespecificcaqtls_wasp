#!/bin/bash
#SBATCH -n 1
#SBATCH -t 24:00:00
#SBATCH --mem 30720

##module add bwa/0.7.15
##module add samtools/1.5
bwa mem -t 4 -M  $1 $2 $3 > $4;

samtools sort -o $5 $4;
samtools index $5;

exit 0;
