#!/bin/bash
## module add fastqc/0.11.5  
## module add multiqc/1.1 
#SBATCH -n 1
#SBATCH -t 24:00:00
#SBATCH -o Fastqc.out
#SBATCH --mem 102400

#echo "FASTQC ATAC_CC9_NYGC ...";
#pfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Progenitors/FASTQ/ATAC_CC9_NYGC/Project_STE_12916_B01_NAN_Lane.2017-09-26 -name "*.R*.fastq.gz"`;
#nfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Neurons/FASTQ/ATAC_CC9_NYGC -name "*.R*.fastq.gz"`;

#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC9_NYGC $pfiles ;
#echo "ATAC_CC9_NYGC progenitor Finished!";
#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC9_NYGC $nfiles ;
#echo "ATAC_CC9_NYGC neuron Finished!";

#echo "FASTQC ATAC_CC10_NYGC ...";
#pfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Progenitors/FASTQ/ATAC_CC10_NYGC -name "*.R*.fastq.gz"`;
#nfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Neurons/FASTQ/ATAC_CC10_NYGC -name "*.R*.fastq.gz"`;

#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC10_NYGC $pfiles ;
#echo "ATAC_CC10_NYGC progenitor Finished!";
#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC10_NYGC $nfiles ;
#echo "ATAC_CC10_NYGC neuron Finished!";
#
#echo "FASTQC ATAC_CC11_NYGC ...";
#pfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Progenitors/FASTQ/ATAC_CC11_NYGC -name "*.R*.fastq.gz"`;
#nfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Neurons/FASTQ/ATAC_CC11_NYGC -name "*.R*.fastq.gz"`;
#
#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC11_NYGC $pfiles ;
#echo "ATAC_CC11_NYGC progenitor Finished!";
#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC11_NYGC $nfiles ;
#echo "ATAC_CC11_NYGC neuron Finished!";


#echo "ATAC_CC12_NYGC";
#pfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Progenitors/FASTQ/ATAC_CC12_NYGC/Project_STE_12917_B01_NAN_Lane.2017-09-26 -name "*.R*.fastq.gz"`;
#nfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Neurons/FASTQ/ATAC_CC12_NYGC -name "*.R*.fastq.gz"`;

#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC12_NYGC $pfiles ;
#echo "ATAC_CC12_NYGC progenitor Finished!";
#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC12_NYGC $nfiles ;
#echo "ATAC_CC12_NYGC neuron Finished!";

#echo "FASTQC ATAC_CC13_NYGC ...";
#pfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Progenitors/FASTQ/ATAC_CC13_NYGC -name "*.R*.fastq.gz"`;
#nfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Neurons/FASTQ/ATAC_CC13_NYGC -name "*.R*.fastq.gz"`;

#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC13_NYGC $pfiles ;
#echo "ATAC_CC13_NYGC progenitor Finished!";
#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC13_NYGC $nfiles ;
#echo "ATAC_CC13_NYGC neuron Finished!";


#echo "FASTQC ATAC_CC14_NYGC ...";
#pfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Progenitors/FASTQ/ATAC_CC14_NYGC -name "*.R*.fastq.gz"`;
#nfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Neurons/FASTQ/ATAC_CC14_NYGC -name "*.R*.fastq.gz"`;

#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC14_NYGC $pfiles ;
#echo "ATAC_CC14_NYGC progenitor Finished!";
#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC14_NYGC $nfiles ;
#echo "ATAC_CC14_NYGC neuron Finished!";


#echo "FASTQC ATAC_CC16_NYGC ...";
#pfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Progenitors/FASTQ/ATAC_CC16_NYGC -name "*.R*.fastq.gz"`;
#nfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Neurons/FASTQ/ATAC_CC16_NYGC -name "*.R*.fastq.gz"`;

#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC16_NYGC $pfiles ;
#echo "ATAC_CC16_NYGC progenitor Finished!";
#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC16_NYGC $nfiles ;
#echo "ATAC_CC16_NYGC neuron Finished!";

#echo "FASTQC ATAC_CC17_NYGC ...";
#pfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Progenitors/FASTQ/ATAC_CC17_NYGC -name "*.R*.fastq.gz"`;
#nfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Neurons/FASTQ/ATAC_CC17_NYGC -name "*.R*.fastq.gz"`;

#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC17_NYGC $pfiles ;
#echo "ATAC_CC17_NYGC progenitor Finished!";
#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC17_NYGC $nfiles ;
#echo "ATAC_CC17_NYGC neuron Finished!";


#echo "FASTQC ATAC_CC18_NYGC ...";
#pfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Progenitors/FASTQ/ATAC_CC18_NYGC -name "*.R*.fastq.gz"`;
#nfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Neurons/FASTQ/ATAC_CC18_NYGC -name "*.R*.fastq.gz"`;

#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC18_NYGC $pfiles ;
#echo "ATAC_CC18_NYGC progenitor Finished!";
#fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC18_NYGC $nfiles ;
#echo "ATAC_CC18_NYGC neuron Finished!";


echo "FASTQC ATAC_CC19_NYGC ...";
pfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Progenitors/FASTQ/ATAC_CC19_NYGC -name "*.R*.fastq.gz"`;
nfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Neurons/FASTQ/ATAC_CC19_NYGC -name "*.R*.fastq.gz"`;

fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC19_NYGC $pfiles ;
echo "ATAC_CC19_NYGC progenitor Finished!";
fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC19_NYGC $nfiles ;
echo "ATAC_CC19_NYGC neuron Finished!";


echo "FASTQC ATAC_CC20_NYGC ...";
pfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Progenitors/FASTQ/ATAC_CC20_NYGC -name "*.R*.fastq.gz"`;
nfiles=`find /proj/steinlab/raw/R00CellCultureQTL/ATAC/Neurons/FASTQ/ATAC_CC20_NYGC -name "*.R*.fastq.gz"`;

fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC20_NYGC $pfiles ;
echo "ATAC_CC20_NYGC progenitor Finished!";
fastqc -o /proj/steinlab/projects/R00/ATACpreprocess/FastQC/fastqc_raw/ATAC_CC20_NYGC $nfiles ;
echo "ATAC_CC20_NYGC neuron Finished!";

exit 0;
