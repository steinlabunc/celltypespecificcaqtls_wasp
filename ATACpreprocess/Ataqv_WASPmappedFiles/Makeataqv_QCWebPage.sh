#!/bin/bash

#SBATCH -n 1
#SBATCH --mem=16g
#SBATCH -t 5:00:00

module add ataqv/1.0.0;

mkarv Summary *.ataqv.json.gz;

exit 0;
