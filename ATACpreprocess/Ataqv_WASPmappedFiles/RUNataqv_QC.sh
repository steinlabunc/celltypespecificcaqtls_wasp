#!/bin/bash

#SBATCH -n 1
#SBATCH --mem=16g
#SBATCH -t 5:00:00

module add ataqv/1.0.0;

ataqv --name $1 --metrics-file $1\.ataqv.json.gz --tss-file ../gencode.v28.hg38.sga.formated.gz --ignore-read-groups human $2 > $1\.ataqv.out;

#ataqv --name R00AL024 --metrics-file R00AL024.ataqv.json.gz --tss-file gencode.v28.hg38.sga.formated.gz --ignore-read-groups human /proj/steinlab/projects/R00/ATACpreprocess/RemvBlacklist/201705310703merged/R00AL24_adaptertrimed_sort_unique_noMT_noblacklist_merged.bam > R00AL024.ataqv.out;

exit 0;
