#!/bin/bash

#SBATCH -n 1
#SBATCH --mem=80g
#SBATCH -t 5:00:00

module add bedtools;


bedtools coverage -b $1 -a ATAC_NeuronProgenitor_Peaks.CHR.bed > $2;

exit 0;
