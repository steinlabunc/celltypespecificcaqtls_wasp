options(stringsAsFactors=F);
##enriched data
GZCP=read.csv("VZCP_NP_all_entichment.csv");
GZ=read.csv("VZ_NP_all_entichment.csv");
CP=read.csv("CP_NP_all_entichment.csv");
##plot data
plotdata=matrix(NA,nrow=2,ncol=3);
colnames=c("GZCP","GZ","CP");
colnames(plotdata)=c("GZCP","GZ","CP");
rownames(plotdata)=c("Neuron","Progenitor");
plotdata[,1]=GZCP[2:3,4];
plotdata[,2]=GZ[,4];
plotdata[,3]=CP[,4];
##plot
pdf("Graph_GZCP_NP_Peaksenrichment.pdf");
barplot(plotdata,legend = rownames(plotdata), beside=TRUE,col=c("deepskyblue1","darkolivegreen3"),ylab="Enrichment");
dev.off();



