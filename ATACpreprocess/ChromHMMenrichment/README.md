Enrichment analysis for neuron peaks and progenitor peaks at ChromMHH based chromtain states
And GZ CP regions
Using GREAT (binormail test)

EpigenomicsRoadmap: http://www.roadmapepigenomics.org/ 

GZCP: https://www.cell.com/cell/fulltext/S0092-8674(17)31494-0

GRAET test: http://bejerano.stanford.edu/papers/GREAT.pdf 
