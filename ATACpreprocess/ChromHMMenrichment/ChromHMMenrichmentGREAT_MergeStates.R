## We use DiffBind R package to do statistical anaysis for all samples
## TMM is used to normalized data
## GREAT is used to do enrichment analysis for each sample
## ChromHMM file to set the chromatin functional states
## module add r/3.6.0
library(GenomicRanges);
library(GenomicFeatures);
library(biomaRt);
library(BSgenome.Hsapiens.UCSC.hg38);
#library(WGCNA);
library(ggplot2);
options(stringsAsFactors=FALSE);
##################################
##Read in progenitors ATAC-seq values into DiffBind
##################################
fDiffbind = "/proj/steinlab/projects/R00/ATACpreprocess/DifferentChromAccess/CSAW/Invitro/Limma/ATAC_NeuronProgenitor_DifferentialPeaks.csv";
Diffbind = read.csv(fDiffbind);
## load Neuron/Progenitor different region csv file and change it to a GRange file
NPbedGR = GRanges(Diffbind$Chr, IRanges(Diffbind$peakstart,Diffbind$peakend));
## add foldchange and FDR information into GRange file
mcols(NPbedGR) = DataFrame(log2FoldChange = Diffbind$logFC,FDR = Diffbind$adj.P.Val);
myseqnames = unique(seqnames(NPbedGR));
NPbedGR = NPbedGR[which(seqnames(NPbedGR) %in% myseqnames)];
NPbedGR = sort(sortSeqlevels(NPbedGR));
## find all significant regions
NPbedGR = NPbedGR[mcols(NPbedGR)$FDR < 0.05];
## find progenitor>neuron (log2FoldChange > 0) open region and  progenitor<neuron (log2FoldChange < 0) open region
#PbedGR = NPbedGR[mcols(NPbedGR)$log2FoldChange > 0];
#NbedGR = NPbedGR[mcols(NPbedGR)$log2FoldChange < 0];
## Load ChromHMM file list
ChromHMMfiles = list.files("/proj/steinlab/projects/R00/ChromHMM/OtherTissue",pattern="*..bed.hg38$",full.names=TRUE);
## put brain files in the list
ChromHMMfiles = c(ChromHMMfiles,"/proj/steinlab/projects/R00/ChromHMM/E081_25_imputed12marks_dense_hg38.bed","/proj/steinlab/projects/R00/ChromHMM/E082_25_imputed12marks_dense.bed.hg38");
##extract file names
names = sapply(ChromHMMfiles,function(x) unlist(strsplit(x,"/",fixed=TRUE))[length(unlist(strsplit(x,"/",fixed=TRUE)))]);
names = sapply(names,function(x) unlist(strsplit(x,"_",fixed=TRUE))[1]);
## 25 states 
#states = c("1_TssA","2_PromU","3_PromD1","4_PromD2","5_Tx5'","6_Tx","7_Tx3'","8_TxWk","9_TxReg","10_TxEnh5'","11_TxEnh3'","12_TxEnhW","13_EnhA1","14_EnhA2","15_EnhAF","16_EnhW1","17_EnhW2","18_EnhAc","19_DNase","20_ZNF/Rpts","21_Het","22_PromP","23_PromBiv","24_ReprPC","25_Quies");
## make a merged states
states = list(Promoter = c("1_TssA","2_PromU","3_PromD1","4_PromD2","22_PromP","23_PromBiv"), Enhancer = c("13_EnhA1","14_EnhA2","15_EnhAF","16_EnhW1","17_EnhW2","18_EnhAc"), Heterochromatin = c("21_Het"), Quiescent = c("25_Quies"), Transcribed=c("5_Tx5'","6_Tx","7_Tx3'","8_TxWk","9_TxReg","10_TxEnh5'","11_TxEnh3'","12_TxEnhW"),Polycomb=c("24_ReprPC"),ZNF_Rpts=c("20_ZNF/Rpts"));
##Number of base pairs in the autosomes
genomelength = sum(as.numeric(seqlengths(Hsapiens)[1:24]));
##Set up output values
orignalpval = matrix(NA,nrow=length(names),ncol=length(states));
colnames(orignalpval) = names(states);
## p-value
pval = matrix(NA,nrow=length(names),ncol=length(states));
colnames(pval) = names(states);
##enrichment of peaks overlapping with states
enrichment = matrix(NA,nrow=length(names),ncol=length(states));
colnames(enrichment) = names(states);
##number of peaks overlapping with states
npeaks = matrix(NA,nrow=length(names),ncol=length(states));
colnames(npeaks) = names(states);
##number of peak bps overlapping with states
npeakbps = matrix(NA,nrow=length(names),ncol=length(states));
colnames(npeakbps) = names(states);
##set row names
rownames(orignalpval) = names;
rownames(enrichment) = names;
rownames(npeaks) = names;
rownames(npeakbps) = names;
for (i in 1:length(ChromHMMfiles)){
     ## Load ChromHMM file
     ChromHMMfile = read.table(ChromHMMfiles[i]);
     colnames(ChromHMMfile)=c("chrom","chromStart","chromEnd","name","score","strand","thickStart","thickEnd","itemRgb");
     ##Loop through each states
     for (j in 1:length(states)) {
    	thisstate = ChromHMMfile[which(ChromHMMfile$name %in% states[[j]]),];
    	thisstateGR = GRanges(thisstate$chrom,IRanges(thisstate$chromStart,thisstate$chromEnd));
        thisstateGR = thisstateGR[which(seqnames(thisstateGR) %in% myseqnames)];
	##Find overlap with thisstateGR
 	olap = findOverlaps(NPbedGR,thisstateGR);
 	numpeaksoverlappromoter = length(unique(queryHits(olap)));
        npeaks[i,j] = numpeaksoverlappromoter;
	##Compute the significance of the enrichment using a binomial test
	##See McLean et al., "GREAT improves functional interpretation of cis-regulatory regions", 2010, Nat Biotech
  	##p.binom = fraction of genome that the state overlaps
  	##n.binom = number of peak calls
  	##s.binom = number of peak calls that overlap the state
        p.binom = sum(as.numeric(width(thisstateGR)))/genomelength;
  	n.binom = length(reduce(NPbedGR,ignore.strand=TRUE));
  	s.binom = numpeaksoverlappromoter;  
        orignalpval[i,j] = pbinom(s.binom-1,n.binom,p.binom,lower.tail=FALSE,log.p=F);
        pval[i,j] = pbinom(s.binom-1,n.binom,p.binom,lower.tail=FALSE,log.p=TRUE) * -log10(exp(1)); 
        #enrichment = (#bases in state AND overlap feature)/(#bases in genome) / [(#bases overlap feature)/(#bases in genome) X (#bases in state)/(#bases in genome)]
        numbpsoverlappromoter = sum(as.numeric(width(reduce(intersect(NPbedGR,thisstateGR)))));
        npeakbps[i,j]=numbpsoverlappromoter;
        enrichment[i,j] = (numbpsoverlappromoter/genomelength) / ((sum(as.numeric(width(NPbedGR)))/genomelength) * p.binom);
}
}

write.csv(orignalpval,"ChromMHHenrichment_MergeStates_pvalues.csv",quote=FALSE,row.names=TRUE);
write.csv(enrichment,"ChromMHHenrichment_MergeStates_enrichment.csv",quote=FALSE,row.names=TRUE);
write.csv(npeaks,"ChromMHHenrichment_MergeStates_peaknumbers.csv",quote=FALSE,row.names=TRUE);
write.csv(pval,"ChromMHHenrichment_MergeStates_log10pvalue.csv",quote=FALSE,row.names=TRUE);
write.csv(npeakbps,"ChromMHHenrichment_MergeStates_peak_bp_numbers.csv",quote=FALSE,row.names=TRUE);
pdf("ChromMHHenrichment_MergeStates_OtherTissue.pdf");
for (i in 1:length(states)){    
    index = order(pval[,i],decreasing = T)[1:50];
    barplot((pval[,i]*sign(enrichment[,i]-1))[index],names.arg=rownames(pval)[index],las=2,ylab="-log10(P-value) GREAT enrichment",cex.names=0.5,main=paste0(names(states)[i]," P-value Enrichment of peaks at states"));
    index = order( log10(enrichment[,i]),decreasing = T)[1:50];
    barplot(log10(enrichment[,i])[index],names.arg=rownames(pval)[index],las=2,ylab="log10(Foldchange)",cex.names=0.5,main=paste0(names(states)[i]," Enrichment of peaks at states"));
    index = order(npeaks[,i],decreasing = T)[1:50];
    barplot(npeaks[,i][index],names.arg=rownames(pval)[index],las=2,ylab="#Peaks",cex.names=0.5,main=paste0(names(states)[i]," Number of peaks at states"));

}       
dev.off();


