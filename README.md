**Cell-type specific effects of genetic variation on chromatin accessibility during human neuronal differentiation**

Dan Liang, Angela L. Elwell, Nil Aygün, Oleh Krupa, Felix A. Kyere, Michael J. Lafferty, Kerry E. Cheek, Kenan P. Courtney, Marianna Yusupova, Melanie E. Garrett, Allison Ashley-Koch, Gregory E. Crawford, Michael I. Love, Luis de la Torre-Ubieta, Daniel H. Geschwind, Jason L. Stein

BioRxiv: https://www.biorxiv.org/content/10.1101/2020.01.13.904862v1
Nat Neuroscience: https://www.nature.com/articles/s41593-021-00858-w


Unfiltered caQTL data can be found in the google drive:
https://drive.google.com/drive/folders/11J43jekEI7D3KlTcLJXLTseiY11BbcGi?usp=share_link

Contact:

Dan Liang: danliang@email.unc.edu

Jason Stein: jason_stein@med.unc.edu
